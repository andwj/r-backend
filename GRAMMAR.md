
SageLeaf GRAMMAR
================

by Andrew Apted, 2023.


Introduction
------------

This document describes the grammar (syntax) of this language.
The grammar is specified using an extended BNF (Backus-Naur form),
where `|` represents a choice, things in `[]` square brackets are
optional, and things in `{}` curly braces may be repeated zero or
more times.  Uppercase names represent low-level tokens.


Overall Format
--------------

The overall format of an input file is textual and line-based.
The encoding must be UTF-8 (the Unicode character set).
Each line is terminated by the `LF` (line feed) character.
The `CR` carriage return may appear before `LF`, and is ignored.
Other control characters act as whitespace.

Each input line is decomposed into a sequence of tokens.
The six characters `()[]{}` always form individual tokens, all
other tokens are delimited by whitespace or one of those symbols.

If the final token on a line is `\` (a backslash), then the logical
line is extended with tokens from the next input line, and this can
repeated multiple times.

The `;` (semicolon) character introduces a comment which extends to
the end of the line, and the whole sequence is equivalent to whitespace.
There is no multi-line comment mechanism.

The three opening braces `(`, `[` or `{` begin a logical group of
tokens.  The group must be terminated by the corresponding closing
brace `)`, `]` or `}`.  Such groups may span multiple lines.

Character literals are delimited by single quote characters.
Strings literals are delimited by double quote characters, and
must be contained on a single line.  These may contain escape
sequences to produce any valid Unicode character.


Input File
----------

```
file := { directive LF }

directive := dir-function
          |  dir-variable
          |  dir-extern
          |  dir-visibility
```


Directives
----------

```
dir-function   := "fun"    { fun-flag } GLOBAL param-set body
dir-extern     := "extern" { ext-flag } GLOBAL
dir-variable   := "var"    { var-flag } GLOBAL "=" data

dir-visibility := "public"
               |  "private"

param-set   := "(" { parameter } return-type ")"
parameter   := LOCAL type
return-type := "->" type
body        := LF { statement LF } "end"

data        := data-single
            |  LF { data-multi } LF "end"
data-single := data-zero | type data-elem
data-multi  := data-zero | type data-elem { data-elem }
data-zero   := "zero" INTEGER
data-elem   := literal | GLOBAL
```


Code Statements
---------------

```
statement := st-label
          |  st-file
          |  st-line
          |  st-assign
          |  st-swap
          |  st-jump
          |  st-call
          |  st-return
          |  st-panic
          |  st-check-null
          |  st-check-range

st-label       := NAME ":"
st-file        := "file" GLOBAL
st-line        := "line" INTEGER [ STRING ]

st-assign      := target "=" computation
st-swap        := "swap" target target
st-jump        := "jump" NAME [ "if" ["not"] computation ]

st-call        := "call" { call-flag } term { term }
st-return      := "ret" [ computation ]

st-panic       := "panic" term
st-check-null  := "check-null"  term
st-check-range := "check-range" INTEGER term
```


Computations
------------

```
computation := cm-call
            |  cm-matches
            |  cm-stack-var
            |  cm-expression
            |  term

cm-call       := "call" type { call-flag } term { term }
cm-matches    := "matches?" ["not"] term { term }
cm-stack-var  := "stack-var" INTEGER

cm-expression := "(" term { expr-op } ")"

expr-op  := unary-op
         |= binary-op  term
         |= convert-op type

unary-op := "ineg"   | "iabs"   | "iflip"
         |  "fneg"   | "fabs"   | "fsqrt"
         |  "izero?" | "isome?" | "ineg?" | "ipos?"
         |  "fzero?" | "fsome?" | "fneg?" | "fpos?"
         |  "fnan?"  | "finf?"
         |  "iswap"  | "ibig"   | "ilittle"
         |  "pnull?" | "pref?"
         |  "bnot"

binary-op := "iadd" | "isub"  | "iand"  | "ior"   | "ixor"
          |  "imul" | "idivt" | "udivt" | "iremt" | "uremt"
          |  "ishl" | "ishr"  | "ushl"  | "ushr"
          |  "imax" | "imin"  | "umax"  | "umin"
          |
          |  "fadd" | "fsub"  | "fmul"  | "fdiv"  | "fremt"
          |  "fmax" | "fmin"
          |  "fround" | "ftrunc" | "ffloor" | "fceil"
          |
          |  "padd" | "psub"  | "pdiff" | "pmin"  | "pmax"
          |  "band" | "bor"   | "bxor"  | "bmin"  | "bmax"
          |
          |  "ieq?" | "ine?"  | "ilt?"  | "ile?"  | "igt?"  | "ige?"
          |  "ueq?" | "une?"  | "ult?"  | "ule?"  | "ugt?"  | "uge?"
          |  "peq?" | "pne?"  | "plt?"  | "ple?"  | "pgt?"  | "pge?"
          |  "feq?" | "fne?"  | "flt?"  | "fle?"  | "fgt?"  | "fge?"
          |  "beq?" | "bne?"  | "blt?"  | "ble?"  | "bgt?"  | "bge?"

convert-op := "iext" | "uext" | "fext"
           |  "ftoi" | "ftou" | "btoi"
           |  "itof" | "utof"
           |  "raw-cast"
```


Terms
-----

```
term := tm-value
     |  tm-local
     |  tm-glob-ptr
     |  tm-memory

target := tm-local
       |  tm-memory

tm-value    := type literal
tm-local    := LOCAL
tm-glob-ptr := GLOBAL

tm-memory   := "[" type mem-base [INTEGER] "]"
mem-base    := LOCAL | GLOBAL
```


Miscellaneous
-------------

```
literal := constant
        |  INTEGER
        |  FLOAT
        |  CHARACTER
        |  STRING

constant := "+INF"
         |  "-INF"
         |  "QNAN"
         |  "SNAN"

type := "i8"   | "i16" | "i32" | "i64"
     |  "f32"  | "f64"
     |  "ptr"  | "iptr"
     |  "void" | "bool"

fun-flag  :=
ext-flag  :=
var-flag  := "rom"

call-flag := "inline"
          |  "no-return"
          |  "tail"
```


Lexical Elements
----------------

Note 1: This grammar is on a character-by-character basis, and
        multiple elements in a rule are not separated by anything
        in the input line.

Note 2: Literals (INTEGER etc) are always matched before a NAME.
        For simplicity the NAME grammar allows things which will
        not actually be matched as an identifier.

Note 3: Hexadecimal floats use decimal digits in the exponent.

```
TOKEN := literal
      |  COMMENT
      |  SYMBOL
      |  LOCAL
      |  GLOBAL
      |  NAME

COMMENT := `;` { any character }
SYMBOL  := `(` | `)`
        |  `[` | `]`
        |  `{` | `}`

LOCAL  := `%` NAME
GLOBAL := `@` NAME

NAME := NAME-CHAR { NAME-CHAR }
NAME-CHAR  := anything except `;` or a SYMBOL

INTEGER := INT-BINARY
        |  INT-HEX
        |  INT-DECIMAL

INT-DECIMAL := [ SIGN ] DIGIT { DIGIT }
INT-BINARY  := [ SIGN ] `0b` BINARY-DIGIT { BINARY-DIGIT }
INT-HEX     := [ SIGN ] `0x` HEX-DIGIT { HEX-DIGIT }

FLOAT := FLOAT-HEX
      |  FLOAT-DECIMAL

FLOAT-DECIMAL := [ SIGN ] DIGIT { DIGIT } `.` { DIGIT } [ EXPONENT ]
              |  [ SIGN ] DIGIT { DIGIT } EXPONENT
EXPONENT      := EXP-E [ SIGN ] DIGIT { DIGIT }
EXP-E         := `e` | `E`

FLOAT-HEX    := [ SIGN ] `0x` HEX-DIGIT { HEX-DIGIT } [ `.` { HEX-DIGIT } ] HEX-EXPONENT
HEX-EXPONENT := EXP-P [ SIGN ] DIGIT { DIGIT }
EXP-P        := `p` | `P`

SIGN := `+` | `-`

STRING      := `"` { STRING-CHAR } `"`
STRING-CHAR := ESCAPE
            |  any character except `"` or `\`

CHARACTER      := `'` CHARACTER-CHAR `'`
CHARACTER-CHAR := ESCAPE
               |  any character except `'` or `\`

ESCAPE := `\"`    ; double quote
       |  `\'`    ; single quote
       |  `\\`    ; backslash
       |  `\a`    ; bell
       |  `\b`    ; backspace
       |  `\t`    ; horizontal tab
       |  `\v`    ; vertical tab
       |  `\f`    ; form feed
       |  `\n`    ; line feed
       |  `\r`    ; carriage return
       |  `\e`    ; escape
       |  OCTAL
       |  HEX
       |  UNICODE

OCTAL       := `\` OCTAL-DIGIT [ OCTAL-DIGIT [ OCTAL-DIGIT ] ]
OCTAL-DIGIT := `0` .. `7`

HEX       := `\` HEX-PAIR
HEX-PAIR  := HEX-DIGIT HEX-PAIR
HEX-QUAD  := HEX-DIGIT HEX-DIGIT HEX-DIGIT HEX-DIGIT
HEX-DIGIT := DIGIT
          |  `a` .. `f`
          |  `A` .. `F`

DIGIT := `0` .. `9`

BINARY-DIGIT := `0` | `1`

UNICODE := `\u` HEX-QUAD
        |  `\U` HEX-QUAD HEX-QUAD
```
