
SageLeaf README
===============

by Andrew Apted, 2023.


About
-----

SageLeaf is a code generator for a low-level assembly-like language.
It's primary purpose is as the back-end to my Rowan programming language,
generating assembly code for NASM to assemble into an object file.

Only the AMD64 (X86-64) architecture is currently supported, though
other architectures are planned.


Documentation
-------------

The [REFERENCE](REFERENCE.md) document contains a lot of detail about
the input files which Sageleaf accepts.



Binary Packages
---------------

TODO


Status
------

TODO


Legalese
--------

SageLeaf is under a permissive MIT license.

SageLeaf comes with NO WARRANTY of any kind, express or implied.

See the [LICENSE.md](LICENSE.md) document for the full terms.


Example
-------

```
;
; Hello world
;

public

fun @main (-> i32)
    call @puts @message
    ret i32 0
end

var rom @message =
    i8 "Hello World!" 0
end

extern @puts
```
