
SageLeaf REFERENCE
==================

by Andrew Apted, 2023.

For the overall syntax, see the accompanying [GRAMMAR](GRAMMAR.md)
document.


Directives
----------

### var

The `var` directive begins a variable definition.  It may be followed by
some flag keywords, then comes the variable name and an `=` equal sign.
After the `=` equal sign may be the data for the variable, otherwise there
should be nothing after it, and the data appears on subsequent lines and
and is terminated by the `end` directive.  Variable names must begin with
the `@` sigil.

See the *Variables* section below for more detail.

### fun

The `fun` directive begins a function definition.  It is followed by the
function name, then the parameters in `()`.  Function names must begin
with the `@` sigil.  Subsequent lines contain the code of the function
(a series of statements and labels).  The definition is terminated by a
line containing the `end` directive.

See the *Functions* section below for more detail.

### extern

The `extern` directive declares a symbol which exists in a different `.o`
object file than the current one.  It is followed by the name of the
variable or function, which must begin with the `@` sigil.  The current
visibility (private or public) has no effect on external symbols.

### private

The `private` directive marks all subsequent variables and functions
as being private (local).  Their symbols in the `.o` object file will
*not* be visible from other `.o` files, and cannot clash with the same
name in another `.o` file.  This is the default visibility of an input
file.

### public

The `public` directive marks all subsequent variables and functions
as being public, i.e. global.  Their symbols in the `.o` object file
*will* be visible from other `.o` files.


------------------------------------------------------------------------------

Types
-----

SageLeaf has a small set of types.  Each global and local variable has a
particular type, and operations on them (such as `iadd`) and assignment
require the types to be compatible.

There are five integer types: `i8`, `i16`, `i32`, `i64` and `iptr`.
The first four have a fixed size (the number represents the number of bits),
whereas `iptr` has the same width as a pointer (and hence depends on the
target architecture).  Operations on these require the same size arguments,
but note that `iptr` is not considered compatible with `i32` or `i64`, even
when it has the same size on a particular architecture.

There are two floating point types: `f32` and `f64`, which are 32-bit and
64-bit respectively.  They will usually use the same format as the IEEE-754
standard for floating point numbers, though this is not guaranteed.
Operations on floats, like `fadd`, require the same size arguments.

The `bool` type represents a single bit, where `0` is considered FALSE,
and `1` is considered TRUE.  The condition of a `jump` statement must have
boolean type.  The size of a boolean is 8 bits, i.e. a byte, though the
jump statement and boolean comparisons are guaranteed to only check the
least significant bit.

The `ptr` type represents a pointer.  The size of a pointer depends on the
target architecture, either 32 or 64 bits.  Exactly what the pointer is
pointing at not specified -- it depends on the context.

The `void` type can only be used as the result of a function, indicating
that the function does not return any value.  Usage in any other context
is an error.


------------------------------------------------------------------------------

Variables
---------

Variables are defined using the `var` directive.  They include a data
specification, which defines the initial value of the variable.

After the `var` directive may appear the `rom` flag.  This causes the
variable data to be placed in the read-only section of the `.o` object
file.  An attempt to write to such a variable is typically caught by the
operating system and raises a "Segmentation Fault" or "General Protection
Fault" error.

In the absence of the `rom` flag, the variable is usually placed in the
normal section for read-write data.  However, when the given data is just
a single `zero` element, then the variable will be stored in the *bss*
section of the executable (used for zeroed data), which reduces the size
of the final executable.

Following `var` (and any flags) is the variable name, which is followed
by an `=` equal sign.  A single data element may follow the `=` equal sign,
otherwise the data appears on subsequent lines and may consist of multiple
data elements.

A same-line example:
```
var @game-over = bool 0
```

An example of multi-line data:
```
var rom @primes =
   i32   2   3   5   7
   i32  11  13  17  19
   i32  23  27  31  37
end
```

An example of a zeroed block:
```
var @buffer = zero 1024
```

For data on the same line as `var`, the data element consists of a type
name followed by a literal value, or the keyword `zero` followed by an
integer to create a block of zeroed data of a given size.

Multi-line data is similar, though when multiple elements of the same type
appear in a sequence, the type name only needs to be given once (at the
beginning of the sequence), though it is okay to repeat the type name.

When the name of a global variable is used in code, or in the data block
of another variable, then it represents the *address* of where the data
is stored.  To read or write the variable requires the use of the memory
reference syntax in `[]` square brackets.


------------------------------------------------------------------------------

Functions
---------

Functions are defined using the `fun` directive.  This is potentially
followed by one or more flags, though none are currently defined.

After `fun` appears the function name, which must begin with the `@` (at)
sigil, since it is a global identifer.  Global identifiers share a single
namespace, hence all functions are variables must have a unique name among
all the input files.

The function name is followed by the function signature, which is the set
of parameters and the return type in `()` parentheses.  Each parameter is a
local name, beginning with the `%` (percent) sigil, followed by a type name
like `i32` or `ptr`.  The return type is the symbol `->` followed by a type
name, often `void` for functions which do not return a value.

The function body appears on lines following the definition, and finishes
with the `end` directive.  The body can be empty, otherwise each logical
line contains a statement.  See the next section for more information
about statements.

Example of a function which adds three integers:
```
fun @sum (%x i32 %y i32 %z i32 -> i32)
    ret (%x iadd %y iadd %z)
end
```


Statements
----------

### file

The `file` statement requires the name of a global variable, which holds a
string for the current source file.  The pointer to this string is passed
to the external panic functions when a panic occurs.

### line

The `line` statement specifies the current line number, which is passed
to the external panic functions when a panic occurs.  A value of zero is
ignored.  The line number may be followed by a string which contains a
comment which will be output into the assembly file, typically used to
show the source statement (or part thereof) which is very handy when
examining the generated assembly code.

### labels

A label is a line containing an identifier immediately followed by a `:`
(colon) character.  Nothing else may appear on the same line.  The label
name can be used as the target of the `jump` statement, and must be
unique within the function.

### assignments

An assignment is a line containing a target followed by a `=` (equal sign)
token and then a computation.  The result of the computation will be
stored in the destination target.  See the sections below for more detail
about targets, and the different kinds of computations.

### call

The `call` statement is very similar to the `call` computation (see below).
Syntactically, the only difference is that no type name appears after the
`call` keyword, because the returned value is never used in this context.
Semantically, the main difference is that the flags `no-return` and `tail`
are allowed to be used here.  See the *Call Flags* section for more detail
about the flag keywords.

### jump

The `jump` statement is the fundamental form of flow control, causing the
the execution of the code in a function to jump to a different part of the
function.  After the `jump` keyword must be a label name.  Nothing else is
needed for an unconditional jump.  Otherwise the label is followed by the
keyword `if`, then an optional keyword `not`, and then a computation.
The computation must have boolean type, and its value (possibly negated
by the `not` keyword) determines whether the jump is performed.

### ret

The `ret` statement returns from a function.  When the current function
has a result type of `void`, then nothing else is needed (or allowed),
otherwise following the `ret` keyword is a computation which computes
the value to return.  This computation could simply be a type name and
literal value, or an expression in `()` -- see the sections below for
more detail about computations.  The type of the computation must match
the result type of the current function.

Note that when a function returns a value, it is an error if the flow of
control can naturally reach the end without a `ret` statement, or a call
to a `no-return` function.  You should be careful in your front-end to
ensure that this cannot happen -- simply adding a `ret` with a default
value at the end is one easy (albeit not very elegant) method.

### swap

The `swap` statement requires a pair of targets, and will swap the values
between them.  The targets may be local variables, or memory references,
and must have the same type.  In certain situations a `swap` statement
will use more efficient CPU instructions, especially when both targets
are local variables which are in registers.

### panic

The `panic` statement requires a term, typically the name of a global var
holding a string for the panic message.  The message plus the current
file/line information are passed to the function "panic" (an external
reference) and that function is reponsible for displaying the message
and aborting the program.

### check-null

The `check-null` statement requires a term which must have `ptr` type,
typically a local variable, and will check whether the pointer is NULL
or not.  When the pointer is NULL, the current file/line information are
passed to the function "panic_null" (an external reference) and that
function is responsible for displaying a message and aborting the
program.

### check-range

The `check-range` statement requires an INTEGER for the number of elements
in the array, and a term which is the index of that array.  The index must
have integral type, and will be considered *unsigned*.  When the index is
greater than or equal to the number of elements, the current file/line
information is passed to a function "panic_range" (an external reference)
and that function is responsible for displaying a message and aborting
the program.


------------------------------------------------------------------------------

Targets
-------

A "target" is something which is a valid destination of an assignment.
It is either the name of a local variable, or a memory reference.  See
the following sections for more information.


Terms
-----

Terms are values used in expressions in `()` parentheses, as well as the
parameters of a function call or the `matches?` computation.  Each term
is one of the following: a local variable, a global variable, a literal
value, or a memory reference.  Global variables have a `@` sigil, and
are described in the Variables section above.  The rest are described in
the following sections....


Literal values
--------------

A literal value consists of a type name, like `i64`, followed by the
literal itself, like `123456`.

Integers may be preceded by a sign, either `+` or `-`, and followed by
one or more decimal digits.  Hexadecimal integers begin with `0x` (after
any sign) and are followed by one or more hex digits.  Binary digits
begin with `0b` and consist of the digits `0` and `1`.

Boolean literals are the type name `bool` followed by an integer which
evaluates to either zero or one, where zero represents FALSE and one
represents TRUE.  Any other value simply ignores the upper bits.

Pointer literals are the type name `ptr` followed by an integer, and that
integer represents a raw address, though it is typically zero to create
a null pointer.

Floating point literals are an optional sign, either `+` or `-`, followed
by at least one decimal digit, followed by a `.` (point) with one or more
fractional digits, and may be finished with an optional exponent.  The
exponent is the letter `e` or `E`, with an optional sign, and then one or
more decimal digits.  When the exponent is present, the fractional part
(beginning with `.`) is optional.

Floating point values may also be hexadecimal.  This is an optional sign,
followed by `0x`, then at least one hexadecimal digit, followed by `.`
(point) with one or more hexadecimal digits for the fractional part, and
*must* be finished with an exponent.  For hex floats, the exponent is the
letter `p` or `P`, with an optional sign, and then one or more *decimal*
digits.  The fractional part (beginning with `.`) is optional.

In addition to the above, a floating point value may be specified "raw",
via an integer literal after the type name.  The integer will be cast
(re-interpreted) as the given floating point type.  This mechanism is
*not* portable and should only be used when absolutely necessary.

There are four special floating point constants:
-  `+INF` represents a positive infinity
-  `-INF` represents a negative infinity
-  `QNAN` represents a quiet NaN (Not a Number)
-  `SNAN` represents a signalling NaN

Note that in the IEEE-754 specification for floating point numbers, there
are many bit patterns which can represent a "NaN" (Not a Number) value.
The `QNAN` and `SNAN` constants are just two common ones.

Exactly what behavior a signalling NaN performs (among other things) can
depend on the CPU architecture, the contents of special CPU registers,
and facilities/conventions of the target operating system.  These details
are beyond the scope of this manual (and SageLeaf itself).


Local Variables
---------------

Local variables only occur in functions, and are identifiers prefixed
by the `%` (percent) sigil.  The parameters of a function are considered
to be local variables.  The name of each local variable must be unique
in each function -- there is no scoping mechanism.

Local variables are created implicitly by an assignment statement.
Each local variable has a type, and subsequent usage of that local,
such as in expressions, assignments, or parameters of a function call,
*must* be compatible with its type.  For example, a local called `%x`
cannot be assigned an integer on one line, and a float somewhere else.
Parameters get an explicit type from the function signature, for other
locals their type is deduced from their initial assignment.

It is possible to construct a function where a local variable is used
without being initialized (by jumping past the initial assignment).
SageLeaf will detect this and create the variable earlier, initializing
it to a zero value.  Nevertheless, I recommend that your front-end be
be careful to avoid such a situation.


Memory References
-----------------

Accessing memory, including global variables, must be done via a memory
reference, which is a sequence in `[]` square brackets.  This sequence
begins with a type name, which represents what is being read or written.
The type is followed by a base pointer, which is either a local variable
with `ptr` type, or the name of any global variable.  The base pointer is
optionally followed by an integer literal, which is the offset (in bytes)
from the base pointer.


Call Flags
----------

### inline

The `inline` flag requests that the function is inlined instead of being
called.  Inlining causes the code of the other function to be directly
incorporated into the current function.  This *can* improve performance
in certain circumstances, but is not guaranteed to do so.

The inlining process will be repeated when the other function also has
calls with the `inline` flag.  However, if an infinite cycle is detected,
then the cycle is broken by turning the problematic call into a normal
function call.

### no-return

The `no-return` flag tells the control-flow analyser that this call will
never return.  It is mainly for functions which exit the program.  This
flag can allow more code to be marked as "dead" (and hence removed), and
a few other optimisations.  If the call actually *does* return, then the
behavior is undefined, but the program is very likely to crash.

### tail

The `tail` flag specifies that the call is a "tail call".  This is a
special kind of call which saves stack space and allowing recursive
algorithms to work without overflowing the stack.  The called function
will not return to he current function, but instead returns to the
caller of the current function.  Due to ABI limitations, a tail-called
function cannot have more than four parameters.


------------------------------------------------------------------------------

Computations
------------

### call

The `call` computation calls a function and produces the returned value.
Following the `call` keyword is a type name, which is the type of the
returned value.  The type cannot be `void`.  After the type name may appear
one or more flag keywords.  These are followed by a term which evaluates to
a pointer, the address of the code to call.  This is typically the name of
a global or external function.  Following the function reference is a term
for each parameter, possibly none.

In this context, the `no-return` and `tail` flags are not meaningful and
hence ignored.  See the *Call Flags* section for more information.

### matches?

The `matches?` computation checks whether a base term matches any of a
group of terms, producing a boolean result.  It requires at least one term
(the base).  If there are no other terms to compare it with, then the match
trivially fails.  The keyword `not` may appear before the base term, which
negates the result.  The type of each comparison term must be compatible
with the base term.

### stack-var

The `stack-var` computation causes a chunk of the local stack frame to be
statically allocated (i.e. at compile time, *not* dynamically).  It must
be followed by an integer literal which is the amount of space needed,
and at run-time it produces a *pointer* to the area of the stack.

Note that the area reserved for the chunk is not initialized or cleared
in any way, that is something which your front-end language can do, or
you can leave it up to the programmer using your language.

### expressions

An expression is a series of operations inside `()` parentheses.  The first
element inside `()` is a base term, which is then followed by zero or more
operations.  Each operation begins with the name of the operation, and may
be followed either by nothing (for unary operations), a term (for binary
operations) or a type name (for conversions).  Each operation modifies the
previous value to produce a new value, and may change the current type too
(especially the conversion operations).

The following sections of this document briefly describe each operation.
Most operation names begin with a letter to signal the type on which it
operates.  The `u` prefix denotes operations on unsigned integers, the `i`
prefix is for operations on signed integers (like `ineg?`) but also for
operations where the signed-ness does not matter (like `iand`).

The operations with names ending in `?` (question mark) perform some kind
of test, and produce a boolean result which is TRUE (`bool 1`) if the test
succeeds and FALSE (`bool 0`) if the test fails.

A few operations are merely synonyms, for example `ushl` and `ishl` perform
the same operation, `ueq?` is equivalent to `ieq?`, and `bmax` is equivalent
to `bor`.  They are provided because they can simplify the selection of
operations in your front-end compiler.

An example expression which converts a pointer to integer and adds
the number three to it:
```
%val = (%base  raw-cast iptr  iadd iptr 3)
```


Unary Operations
----------------

- `ineg`  : negates a signed integer
- `iabs`  : produces absolute value of a signed integer
- `iflip` : flip (toggle) all bits in an integer

- `fneg`  : negates a float
- `fabs`  : produces absolute value of a float
- `fsqrt` : produces square root of a float >= 0, undefined otherwise

- `izero?` : test if integer is zero
- `isome?` : test if integer is non-zero
- `ineg?`  : test if signed integer is negative (< 0)
- `ipos?`  : test if signed integer is positive (> 0)

- `fzero?` : test if float is zero
- `fsome?` : test if float is non-zero
- `fneg?`  : test if float is negative (< 0)
- `fpos?`  : test if float is positive (> 0)
- `fnan?`  : test if float is a NaN (Not a Number)
- `finf?`  : test if float is an infinity

- `pnull?` : test if pointer is NULL (zero)
- `pref?`  : test if pointer is non-NULL

- `iswap`   : swap bytes in an integer
- `ibig`    : swap bytes if CPU is little endian, otherwise do nothing
- `ilittle` : swap bytes if CPU is big endian, otherwise do nothing

- `bnot`    : boolean negation


Binary Operations
-----------------

- `iadd`  : integer addition
- `isub`  : integer subtraction
- `imul`  : integer multiplication
- `idivt` : signed integer division    (truncating)
- `iremt` : signed integer remainder   (truncating)
- `udivt` : unsigned integer division  (truncating)
- `uremt` : unsigned integer remainder (truncating)

- `imax`  : maximum of two signed integers
- `imin`  : minimum of two signed integers
- `umax`  : maximum of two unsigned integers
- `umin`  : minimum of two unsigned integers

- `iand`  : integer bit-wise and
- `ior`   : integer bit-wise or
- `ixor`  : integer bit-wise xor (exclusive or)
- `ishl`  : signed integer shift left
- `ishr`  : signed integer shift right
- `ushl`  : unsigned integer shift left
- `ushr`  : unsigned integer shift right

- `padd`  : add integer to a pointer
- `psub`  : subtract integer from a pointer
- `pdiff` : difference of two pointers, result is integer
- `pmax`  : maximum of two pointers
- `pmin`  : minimum of two pointers

- `fadd`  : float addition
- `fsub`  : float subtraction
- `fmul`  : float multiplication
- `fdiv`  : float division
- `fremt` : float remainder, via truncated division
- `fmax`  : maximum of two floats
- `fmin`  : minimum of two floats

- `fround` : round float to nearest whole number
- `ftrunc` : round float toward zero (truncate it)
- `ffloor` : round float down (toward -infinity)
- `fceil`  : round float up   (toward +infinity)

- `band`  : boolean and
- `bor`   : boolean or
- `bxor`  : boolean xor (exclusive or)
- `bmax`  : maximum of two booleans
- `bmin`  : minimum of two booleans


Comparisons
-----------

- `ieq?` : test if signed integer X is equal to Y
- `ine?` : test if signed integer X is not equal to Y
- `ilt?` : test if signed integer X is less than Y
- `ile?` : test if signed integer X is less or equal to Y
- `igt?` : test if signed integer X is greater than Y
- `ige?` : test if signed integer X is greater or equal to Y

- `ueq?` : test if unsigned integer X is equal to Y
- `une?` : test if unsigned integer X is not equal to Y
- `ult?` : test if unsigned integer X is less than Y
- `ule?` : test if unsigned integer X is less or equal to Y
- `ugt?` : test if unsigned integer X is greater than Y
- `uge?` : test if unsigned integer X is greater or equal to Y

- `feq?` : test if float X is equal to Y
- `fne?` : test if float X is not equal to Y
- `flt?` : test if float X is less than Y
- `fle?` : test if float X is less or equal to Y
- `fgt?` : test if float X is greater than Y
- `fge?` : test if float X is greater or equal to Y

- `peq?` : test if pointer X is equal to Y
- `pne?` : test if pointer X is not equal to Y
- `plt?` : test if pointer X is less than Y
- `ple?` : test if pointer X is less or equal to Y
- `pgt?` : test if pointer X is greater than Y
- `pge?` : test if pointer X is greater or equal to Y

- `beq?` : test if boolean X is equal to Y
- `bne?` : test if boolean X is not equal to Y
- `blt?` : test if boolean X is less than Y
- `ble?` : test if boolean X is less or equal to Y
- `bgt?` : test if boolean X is greater than Y
- `bge?` : test if boolean X is greater or equal to Y


Conversions
-----------

- `iext` : extend or narrow a signed integer
- `uext` : extend or narrow an unsigned integer
- `fext` : extend or narrow a float

- `ftoi` : convert float to signed integer
- `ftou` : convert float to unsigned integer
- `itof` : convert signed integer to float
- `utof` : convert unsigned integer to float
- `btoi` : convert boolean to integer

- `raw-cast` : raw cast between two types of the same size

