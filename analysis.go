// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// CodePath represents a path of code statements in a function.
// The path may be empty, it represents a function which has not yet
// started execution.
type CodePath struct {
	// the next node to be visited, -1 if finished
	next int

	// nodes in the path (indexes into body)
	visited map[int]bool

	// the locals which have been created along this path.
	locals map[*LocalVar]bool

	// this path produced an error
	failed bool
}

func NewCodePath() *CodePath {
	pt := new(CodePath)
	pt.next    = 0
	pt.visited = make(map[int]bool, 0)
	pt.locals  = make(map[*LocalVar]bool)
	return pt
}

func (pt *CodePath) Copy() *CodePath {
	pt2 := new(CodePath)
	pt2.next = pt.next

	pt2.visited = make(map[int]bool)
	for idx,_ := range pt.visited {
		pt2.visited[idx] = true
	}
	pt2.locals = make(map[*LocalVar]bool)
	for loc,_ := range pt.locals {
		pt2.locals[loc] = true
	}
	return pt2
}

func (pt1 *CodePath) CanMerge(pt2 *CodePath) bool {
	return pt1.next == pt2.next
}

func (pt *CodePath) Merge(old *CodePath) {
	for idx,_ := range old.visited {
		pt.visited[idx] = true
	}

	// only keep locals that are common to both paths
	new_locals := make(map[*LocalVar]bool)
	for loc, _ := range old.locals {
		if pt.locals[loc] {
			new_locals[loc] = true
		}
	}

	pt.locals = new_locals
	pt.failed = pt.failed || old.failed

	// mark old path as dead
	old.next = -1
}

func (fu *FuncDef) FlowAnalysis() error {
	// begin by marking all nodes as dead.
	// the flag is cleared when a path reaches a node.
	for _, t := range fu.body.children {
		t.flags = t.flags | NF_Dead
	}

	// create initial path
	pt := NewCodePath()

	fu.paths = make([]*CodePath, 0)
	fu.paths = append(fu.paths, pt)

	error_count := 0

	/* process active paths until done */

	for len(fu.paths) > 0 {
		// remove dead paths from the end
		last := len(fu.paths) - 1
		pt   := fu.paths[last]

		if pt.next < 0 {
			if pt.failed {
				error_count += 1
			}
			fu.paths = fu.paths[0:last]
			continue
		}

		// choose the path at earliest node
		for i := 0; i < last; i++ {
			other := fu.paths[i]
			if other.next >= 0 && other.next < pt.next {
				pt = other
			}
		}

		// attempt to merge other paths into this one
		for i := 0; i <= last; i++ {
			other := fu.paths[i]
			if other != pt {
				if pt.CanMerge(other) {
					pt.Merge(other)
				}
			}
		}

		fu.FlowStep(pt)
	}

	if error_count > 0 {
		return FAILED
	}
	return OK
}

func (fu *FuncDef) FlowStep(pt *CodePath) {
	// reached end?
	if pt.next >= fu.body.Len() {
		ret_type := fu.ret_type

		if ret_type.kind != TYP_Void && !fu.missing_ret {
			fu.missing_ret = true
			last := fu.body.Last()
			Error_SetPos(last.pos)
			PostError("control reaches end of non-void function")
		}
		pt.next = -1
		return
	}

	// already visited? (i.e. looping back)
	if pt.visited[pt.next] {
		pt.next = -1
		return
	}

	pt.visited[pt.next] = true

	t := fu.body.children[pt.next]

	// it's alive!!
	t.flags = t.flags & (^ NF_Dead)

	// check for use of uninitialized locals
	err := fu.FlowCheckLocals(t, pt)
	if err != nil {
		pt.failed = true
	}

	// check for creation of locals
	if t.kind == NH_Move {
		if t.children[0].kind == NP_Local {
			local := t.children[0].local
			pt.locals[local] = true
		}
	}

	// handle return statements, or equivalent
	switch t.kind {
	case NH_Return, NH_Panic:
		pt.next = -1
		return

	case NH_Drop:
		// calling a `no-return` function?
		comp := t.children[0]

		if comp.kind == NC_Call {
			if (comp.flags & NF_NoReturn) > 0 {
				pt.next = -1
				return
			}
		}
		if comp.kind == NC_TailCall {
			pt.next = -1
			return
		}
	}

	// handle jumps.
	// conditional jumps split the path into two.

	if t.kind == NH_Jump {
		dest_idx := fu.FindLabel(t.str)

		// WISH: detect when a jump is not really conditional, especially in
		//       loops where a local begins at zero and stops at a constant.

		if t.Len() > 0 {
			branch := pt.Copy()
			branch.next = pt.next + 1
			fu.paths = append(fu.paths, branch)
		}

		pt.next = dest_idx
		return
	}

	pt.next += 1
}

func (fu *FuncDef) FlowCheckLocals(t *Node, pt *CodePath) error {
	if t.kind == NP_Local {
		local := t.local

		// it has been created in this path?
		if pt.locals[local] {
			return OK
		}
		// parameters don't need a previous write
		if local.param_idx >= 0 {
			return OK
		}

		// FIXME local might be used uninitialized, mark it as needing a "lift"

		pt.failed = true
		return FAILED
	}

	// avoid destination of a NH_Move (it is not a read)
	skip := -1
	if t.kind == NH_Move && t.children[0].kind == NP_Local {
		skip = 0
	}

	for i, child := range t.children {
		if i != skip {
			if fu.FlowCheckLocals(child, pt) != OK {
				return FAILED
			}
		}
	}

	return OK
}

//----------------------------------------------------------------------

func (fu *FuncDef) DetectLoops() {
	level := 0

	for {
		found := false

		for idx, line := range fu.body.children {
			if line.kind == NH_Jump {
				if fu.MarkLoop(idx, line.str, level) {
					found = true
				}
			}
		}

		if !found {
			return
		}

		level += 1
	}
}

func (fu *FuncDef) MarkLoop(idx int, label string, level int) bool {
	start := fu.FindLabel(label)

	if start >= idx {
		return false
	}

	children := fu.body.children

	// check that the loop is truly "inside" another one.
	next := idx
	if level > 0 {
		next += 1
	}

	if next < len(children) &&
		children[start+1].loop >= level &&
		children[next].loop >= level {

		// do not mark the label itself, to prevent two loops in
		// a row looking like one big loop.
		for k := start+1; k <= idx; k++ {
			fu.body.children[k].loop = level+1
		}
		return true
	}

	return false
}

//----------------------------------------------------------------------

// loop multipliers for .reads and .writes
var outer_factor = 10
var inner_factor = 40

func (fu *FuncDef) LiveRanges(ignore_updates bool) {
	for _, local := range fu.locals {
		local.reads  = 0
		local.writes = 0
		local.live_start = -1
		local.live_end   = -1
	}

	for cur_line, t := range fu.body.children {
		loop_factor := 1
		if t.loop == 1 {
			loop_factor = outer_factor
		} else if t.loop >= 2 {
			loop_factor = inner_factor
		}

		var skip_node  int = -1
		var skip_local *LocalVar = nil

		if t.kind == NH_Move {
			dest := t.children[0]

			if dest.kind == NP_Local {
				local := dest.local
				local.writes += loop_factor

				fu.UpdateLiveRange(local, cur_line)

				// avoid this destination (it is not a read)
				skip_node = 0

				// when determining unused locals, a statement like `x = iadd x 1`
				// should not be considered as reading from ("using") the local x.
				// however function calls (etc) must not be removed.
				if ignore_updates {
					comp := t.children[1]
					if comp.kind != NC_Call {
						skip_local = local
					}
				}
			}
		}

		if t.kind == NH_Swap {
			for _, child := range t.children {
				if child.kind == NP_Local {
					local := child.local
					local.writes += loop_factor

					fu.UpdateLiveRange(local, cur_line)
				}
			}
		}

		fu.LiveRangesInNode(t, skip_node, cur_line, loop_factor, skip_local)
	}

	// mark all parameters to be live at first node
	for i := 0 ; i < fu.params ; i++ {
		local := fu.locals[i]
		local.live_start = 0
		if local.live_end < 0 {
			local.live_end = 0
		}
	}
}

func (fu *FuncDef) LiveRangesInNode(t *Node, skip, cur_line, loop_factor int, skip_local *LocalVar) {
	if t.kind == NP_Local {
		local := t.local
		if local != skip_local {
			local.reads += loop_factor
		}
		fu.UpdateLiveRange(local, cur_line)
	}

	for pos, child := range t.children {
		if pos != skip {
			fu.LiveRangesInNode(child, -1, cur_line, loop_factor, skip_local)
		}
	}
}

func (fu *FuncDef) UpdateLiveRange(local *LocalVar, cur_line int) {
	if local.live_start < 0 {
		local.live_start = cur_line
		local.live_end   = cur_line

	} else if cur_line < local.live_start {
		local.live_start = cur_line

	} else if cur_line > local.live_end {
		local.live_end = cur_line
	}
}

func (fu *FuncDef) ExtendLiveRanges() {
	// this detects whether a local's last usage is inside a loop,
	// which means the live range may need to extend to the end of
	// that loop unless the local only exists solely in that loop.
	//
	// similarly, if a local is created inside a loop but used by code
	// beyond the end of the loop, its live range needs to be extended
	// to the start of that loop.

	for _, local := range fu.locals {
		if local.live_end >= 0 {
			new_end   := fu.TryExtendForward(local)
			new_start := fu.TryExtendBackward(local)

			local.live_start = new_start
			local.live_end   = new_end
		}
	}
}

func (fu *FuncDef) TryExtendForward(local *LocalVar) int {
	loop := fu.body.children[local.live_end].loop

	// when `loop` is two or more, the local is technically in multiple
	// loops at the same time, and each loop may need to extend it.

	for ; loop > 0; loop-- {
		// find range of the loop
		start := fu.FindLoopStart(local.live_end, loop)
		end   := fu.FindLoopEnd  (local.live_end, loop)

		// is the local wholly contained in this loop?
		if local.param_idx < 0 && local.live_start >= start {
			break
		}

		// WISH: if we are 100% sure that this local is written to near start
		//       of loop (without reading it), with no jumps/labels in between,
		//       then no need to extend the local for this loop.

		// extend the local
		local.live_end = end
	}

	return local.live_end
}

func (fu *FuncDef) TryExtendBackward(local *LocalVar) int {
	if local.param_idx >= 0 {
		return 0
	}

	loop := fu.body.children[local.live_start].loop

	for ; loop > 0; loop-- {
		// find range of the loop
		start := fu.FindLoopStart(local.live_start, loop)
		end   := fu.FindLoopEnd  (local.live_start, loop)

		// is the local wholly contained in this loop?
		if local.live_end <= end {
			break
		}

		// extend the local
		local.live_start = start
	}

	return local.live_start
}

func (fu *FuncDef) FindLoopStart(pos int, level int) int {
	for pos > 0 {
		t := fu.body.children[pos-1]

		// any node with .loop >= level is considered part of the loop
		if t.loop < level {
			break
		}
		pos -= 1
	}
	return pos
}

func (fu *FuncDef) FindLoopEnd(pos int, level int) int {
	for pos+1 < fu.body.Len() {
		t := fu.body.children[pos+1]

		// any node with .loop >= level is considered part of the loop
		if t.loop < level {
			break
		}
		pos += 1
	}
	return pos
}
