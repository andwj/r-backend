// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"

func (fu *FuncDef) ParseComputation() *Node {
	if len(fu.tok) == 0 {
		PostError("missing computation")
		return P_FAIL
	}

	comp := fu.Comp_Main()
	if comp == P_FAIL {
		return P_FAIL
	}

	if len(fu.tok) > 0 {
		PostError("extra rubbish after computation")
		return P_FAIL
	}

	return comp
}

func (fu *FuncDef) Comp_Main() *Node {
	// we don't "eat" this token until we know it is not a term
	head := fu.tok[0]

	if head.kind == NG_Expr {
		fu.tok = fu.tok[1:]
		return fu.Comp_Expression(head)
	}

	if fu.MatchWord("call")      { return fu.Comp_FunCall(true) }
	if fu.MatchWord("matches?")  { return fu.Comp_Matches()  }
	if fu.MatchWord("stack-var") { return fu.Comp_StackVar() }

	return fu.ParseTerm()
}

func (fu *FuncDef) Comp_FunCall(need_a_type bool) *Node {
	ret_type := void_type

	if need_a_type {
		if len(fu.tok) == 0 {
			PostError("missing type after call")
			return P_FAIL
		}
		t_type := fu.tok[0]
		fu.tok  = fu.tok[1:]

		ret_type = ParseType(t_type)
		if ret_type == nil {
			return P_FAIL
		}
		if ret_type.kind == TYP_Void {
			PostError("cannot use void in a computation")
			return P_FAIL
		}
	}

	var flags NodeFlag = 0

	for {
		if fu.MatchWord("inline") {
			flags = flags | NF_Inline
			continue
		}
		if fu.MatchWord("no-return") {
			flags = flags | NF_NoReturn
			continue
		}
		if fu.MatchWord("tail") {
			flags = flags | NF_Tail
			continue
		}
		break
	}

	// ignore flags which make no sense in a computation
	if need_a_type {
		flags = flags & NF_Inline
	}

	if len(fu.tok) == 0 {
		PostError("missing function after call")
		return P_FAIL
	}

	t_func := fu.ParseTerm()
	if t_func == P_FAIL {
		return P_FAIL
	}
	if t_func.ty.kind != TYP_Pointer {
		PostError("function must be a pointer")
		return P_FAIL
	}

	// check inlining, record the dependency
	if (flags & NF_Inline) > 0 {
		if t_func.kind != NP_GlobPtr {
			PostError("inline requires a global function")
			return P_FAIL
		}
		def := t_func.def
		if def.kind != DEF_Func {
			PostError("inline requires a global function")
			return P_FAIL
		}
		fu.inline_deps[def.d_func] = true
	}

	t_call := NewNode(NC_Call, "", t_func.pos)
	t_call.ty = ret_type
	t_call.flags = flags
	t_call.Add(t_func)

	// parse parameters...

	for len(fu.tok) > 0 {
		term := fu.ParseTerm()
		if term == P_FAIL {
			return P_FAIL
		}
		t_call.Add(term)
	}

	// for a global function, check # of parameters
	if t_func.kind == NP_GlobPtr {
		if t_func.def.kind == DEF_Func {
			other := t_func.def.d_func
			if other.params != t_call.Len()-1 {
				PostError("wrong number of parameters in call")
				return P_FAIL
			}
		}
	}

	return t_call
}

func (fu *FuncDef) Comp_Matches() *Node {
	negate := fu.MatchWord("not")

	if len(fu.tok) == 0 {
		PostError("missing terms after matches?")
		return P_FAIL
	}

	t_base := fu.ParseTerm()
	if t_base == P_FAIL {
		return P_FAIL
	}

	// nothing to match against?  then it trivially fails
	if len(fu.tok) == 0 {
		res := NewNode(NL_Integer, "0", t_base.pos)
		res.ty = bool_type

		if negate {
			res.str = "1"
		}
		return res
	}

	t_match := NewNode(NC_Matches, "", t_base.pos)
	t_match.ty = bool_type
	t_match.Add(t_base)

	if negate {
		t_match.flags = t_match.flags | NF_Not
	}

	// grab remaining terms, check types
	for len(fu.tok) > 0 {
		term := fu.ParseTerm()
		if term == P_FAIL {
			return P_FAIL
		}

		if ! TypeCompare(term.ty, t_base.ty) {
			PostError("type mismatch with terms in matches?")
			return P_FAIL
		}

		t_match.Add(term)
	}

	return t_match
}

func (fu *FuncDef) Comp_StackVar() *Node {
	if len(fu.tok) == 0 {
		PostError("missing size after stack-var")
		return P_FAIL
	}

	t_size := fu.tok[0]
	fu.tok  = fu.tok[1:]

	if t_size.kind != NL_Integer {
		PostError("expected integer after stack-var")
		return P_FAIL
	}

	size, err := strconv.ParseInt(t_size.str, 0, 64)
	if err != nil || size < 1 {
		PostError("invalid size for stack-var")
		return P_FAIL
	}

	t_stackvar := NewNode(NC_StackVar, "", t_size.pos)
	t_stackvar.ty = ptr_type
	t_stackvar.offset = size

	return t_stackvar
}

//----------------------------------------------------------------------

func (fu *FuncDef) Comp_Expression(t *Node) *Node {
	if len(fu.tok) > 0 {
		PostError("extra rubbish after computation")
		return P_FAIL
	}

	// prepare to parse the contents in `()`
	fu.tok = t.children

	if len(fu.tok) == 0 {
		PostError("empty expression in ()")
		return P_FAIL
	}

	t_base := fu.ParseTerm()
	if t_base == P_FAIL {
		return P_FAIL
	}

	t_opseq := NewNode(NC_OpSeq, "", t.pos)
	t_opseq.ty = t_base.ty
	t_opseq.Add(t_base)

	for len(fu.tok) > 0 {
		op := fu.ParseOperation(t_opseq.ty)
		if op == P_FAIL {
			return P_FAIL
		}
		if op.str != "nop" {
			t_opseq.Add(op)
			t_opseq.ty = op.ty
		}
	}

	// no actual operations?
	if t_opseq.Len() == 1 {
		return t_opseq.children[0]
	}

	return t_opseq
}

func (fu *FuncDef) ParseOperation(cur_type *Type) *Node {
	t_name := fu.tok[0]
	fu.tok  = fu.tok[1:]

	if t_name.kind != NL_Name || t_name.sigil != 0 {
		PostError("expected operation, got: %s", t_name.String())
		return P_FAIL
	}

	info := FindOperation(t_name.str)
	if info == 0 {
		PostError("unknown operation: %s", t_name.str)
		return P_FAIL
	}

	// check the input type is compatible
	if t_name.str == "raw-cast" {
		// ok
	} else {
		need_type := OperationType(info)

		if cur_type.kind != need_type {
			PostError("operation %s requires a %s", t_name.str, need_type.String())
			return P_FAIL
		}
	}

	op := NewNode(NCX_Operator, t_name.str, t_name.pos)

	if (info & OP_CMP) != 0 {
		op.ty = bool_type
	} else if t_name.str == "pdiff" {
		op.ty = iptr_type
	} else {
		op.ty = cur_type
	}

	is_binary  := (info & OP_BINARY) != 0
	is_convert := (info & OP_CONV)   != 0

	if is_binary {
		return fu.Expr_Binary(op, cur_type, info)
	}
	if is_convert {
		return fu.Expr_Convert(op, cur_type)
	}

	return op
}

func (fu *FuncDef) Expr_Binary(op *Node, cur_type *Type, info OperationInfo) *Node {
	if len(fu.tok) == 0 {
		PostError("missing term after %s", op.str)
		return P_FAIL
	}
	term := fu.ParseTerm()
	if term == P_FAIL {
		return P_FAIL
	}

	// check the types....
	if op.str == "padd" || op.str == "psub" {
		if ! TypeCompare(term.ty, iptr_type) {
			PostError("second arg of %s must be iptr", op.str)
			return P_FAIL
		}

	} else if (info & OP_SHIFT) != 0 {
		if ! TypeCompare(term.ty, i8_type) {
			PostError("second arg of %s must be i8", op.str)
			return P_FAIL
		}

	} else {
		if ! TypeCompare(term.ty, cur_type) {
			PostError("type mismatch with operation %s", op.str)
			return P_FAIL
		}
	}

	op.Add(term)
	return op
}

func (fu *FuncDef) Expr_Convert(op *Node, cur_type *Type) *Node {
	if len(fu.tok) == 0 {
		PostError("missing type name after %s", op.str)
		return P_FAIL
	}
	new_type := ParseType(fu.tok[0])
	if new_type == nil {
		return P_FAIL
	}
	fu.tok = fu.tok[1:]

	// check the type makes sense....
	op.ty = new_type

	if new_type.kind == TYP_Void {
		PostError("cannot convert/cast to void")
		return P_FAIL
	}

	if op.str == "raw-cast" {
		// require same size and archy-ness
		if new_type.archy != cur_type.archy {
			PostError("cannot raw-cast between a fixed-size type and arch-dependent")
			return P_FAIL
		}
		if new_type.bits != cur_type.bits {
			PostError("raw-cast requires types with same size")
			return P_FAIL
		}
		if new_type.kind == cur_type.kind {
			op.str = "nop"
		}
	} else {
		if new_type.kind == TYP_Pointer {
			PostError("cannot convert to pointer type")
			return P_FAIL
		}
		if new_type.kind == TYP_Bool {
			PostError("cannot convert to bool type")
			return P_FAIL
		}
	}

	return op
}

//----------------------------------------------------------------------

func (fu *FuncDef) ParseTerm() *Node {
	if len(fu.tok) == 0 {
		PostError("missing term")
		return P_FAIL
	}
	return fu.Term_Main()
}

func (fu *FuncDef) ParseTarget(new_locals bool) *Node {
	if len(fu.tok) == 0 {
		PostError("missing target")
		return P_FAIL
	}

	t     := fu.tok[0]
	fu.tok = fu.tok[1:]

	if t.sigil == '%' {
		if new_locals && fu.FindLocal(t.str) == nil {
			return fu.Term_NewLocal(t)
		}
		return fu.Term_Local(t)
	}

	if t.kind == NG_Access {
		return fu.Term_Access(t)
	}

	PostError("expected target, got: %s", t.String())
	return P_FAIL
}

func (fu *FuncDef) Term_Main() *Node {
	t     := fu.tok[0]
	fu.tok = fu.tok[1:]

	Error_SetPos(t.pos)

	if t.IsLiteral() {
		PostError("literal without a preceding type name")
		return P_FAIL
	}

	if t.kind == NL_Name {
		return fu.Term_Name(t)
	}
	if t.kind == NG_Access {
		return fu.Term_Access(t)
	}

	PostError("expected term, got: %s", t.String())
	return P_FAIL
}

func (fu *FuncDef) Term_Name(t *Node) *Node {
	switch t.sigil {
	case '%':
		return fu.Term_Local(t)

	case '@':
		return fu.Term_Global(t)
	}

	lit_type := ParseType(t)
	if lit_type == nil {
		return P_FAIL
	}

	if len(fu.tok) == 0 {
		PostError("missing item after %s", t.str)
		return P_FAIL
	}

	t_item := fu.tok[0]
	fu.tok  = fu.tok[1:]

	if t_item.IsLiteral() {
		return TypedLiteral(t_item, lit_type)
	}

	PostError("expected literal after %s", t.str)
	return P_FAIL
}

func (fu *FuncDef) Term_Local(t *Node) *Node {
	local := fu.FindLocal(t.str)
	if local == nil {
		PostError("no such local: %%%s", t.str)
		return P_FAIL
	}
	// has an earlier error preventing it getting a type?
	if local.ty == nil {
		return P_FAIL
	}

	t_loc := NewNode(NP_Local, "", t.pos)
	t_loc.ty = local.ty
	t_loc.local = local

	return t_loc
}

func (fu *FuncDef) Term_NewLocal(t *Node) *Node {
	local := fu.NewLocal(t.str, nil)

	t_loc := NewNode(NP_Local, "", t.pos)
	t_loc.local = local

	return t_loc
}

func (fu *FuncDef) Term_Global(t *Node) *Node {
	def := all_defs[t.str]
	if def == nil {
		PostError("no such global: @%s", t.str)
		return P_FAIL
	}

	t_glob := NewNode(NP_GlobPtr, "", t.pos)
	t_glob.ty  = ptr_type
	t_glob.def = def

	return t_glob
}

func (fu *FuncDef) Term_Access(t *Node) *Node {
	if t.Len() < 2 {
		PostError("missing type or base in []")
		return P_FAIL
	}
	if t.Len() > 3 {
		PostError("extra rubbish in []")
		return P_FAIL
	}

	t_type := t.children[0]
	t_base := t.children[1]

	if ! (t_base.sigil == '%' || t_base.sigil == '@') {
		PostError("bad base in [] -- expected local or global")
		return P_FAIL
	}

	t_base = fu.Term_Name(t_base)
	if t_base == P_FAIL {
		return P_FAIL
	}

	ty := ParseType(t_type)

	if ty == nil {
		return P_FAIL
	}
	if ty.kind == TYP_Void {
		PostError("cannot use void in that context")
		return P_FAIL
	}

	t_mem := NewNode(NP_Memory, "", t.pos)
	t_mem.ty = ty
	t_mem.Add(t_base)

	// check for optional integer offset
	if t.Len() >= 3 {
		t_offset := t.children[2]

		if t_offset.kind == NL_Integer {
			offset, err := strconv.ParseInt(t_offset.str, 0, 64)
			if err != nil {
				PostError("invalid offset in []")
				return P_FAIL
			}

			t_mem.offset = offset
		}
	}

	return t_mem
}

//----------------------------------------------------------------------

func ParamTypesInCall(t_call *Node) []*Type {
	types := make([]*Type, 0, t_call.Len())
	for _, t_par := range t_call.children[1:] {
		types = append(types, t_par.ty)
	}
	return types
}
