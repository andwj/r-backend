// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "sort"

var all_defs map[string]*Definition
var ord_defs []*Definition

type Definition struct {
	kind  DefKind

	d_var    *VarDef
	d_func   *FuncDef
	d_extern *ExternDef
}

type DefKind int
const (
	DEF_Var DefKind = iota
	DEF_Func
	DEF_Extern
)

type ExternDef struct {
	name     string
	out_name string
}

type VarDef struct {
	name     string
	out_name string

	public   bool
	rom      bool
	zeroed   bool

	body  *Node
	data  *Node

	// parsing info
	cur_type *Type
}

type FuncDef struct {
	name     string
	out_name string

	public   bool

	body  *Node

	params    int
	par_types []*Type
	ret_type  *Type

	// the parameters are at beginning of this array
	locals    []*LocalVar
	loc_map   map[string]int
	labels    map[string]bool

	// parsing position in current line
	tok []*Node

	// inline info
	inline_deps map[*FuncDef]bool
	inline_seen map[*FuncDef]bool

	// optimisation info....
	paths  []*CodePath
	missing_ret bool

	ordered_locals []*LocalVar
}

type LocalVar struct {
	name string
	ty   *Type

	// this is >= 0 for parameters, -1 for local vars
	param_idx int

	// these keep track of usages of a local var.
	// the `writes` field includes the creation of the var.
	reads  int
	writes int

	// estimated cost for being on the stack
	cost int64

	// live range of local (indices into the function body).
	// to account for loops, both ends may be extended.
	live_start int
	live_end   int

	// register of a parameter (as per the ABI)
	abi_reg Register

	// register assignment, for REG_ON_STACK the offset is valid
	reg    Register
	offset int
}

//----------------------------------------------------------------------

func (def *Definition) Name() string {
	switch def.kind {
	case DEF_Var:    return def.d_var.name
	case DEF_Func:   return def.d_func.name
	case DEF_Extern: return def.d_extern.name
	default:         return ""
	}
}

func (def *Definition) IsPublic() bool {
	switch def.kind {
	case DEF_Var:  return def.d_var.public
	case DEF_Func: return def.d_func.public
	default:       return true
	}
}

//----------------------------------------------------------------------

func ValidateName(t *Node, what string, sigil rune) error {
	if t.kind != NL_Name {
		PostError("bad %s name: not an identifier", what)
		return FAILED
	}

	if t.sigil != sigil {
		PostError("bad %s name: it must begin with a '%c'", what, sigil)
	}

	name := t.str

	if sigil == '@' {
		if all_defs[name] != nil {
			PostError("bad %s name: @%s already defined", what, name)
			return FAILED
		}
	}

	return OK
}

//----------------------------------------------------------------------

func InitDefs() {
	all_defs = make(map[string]*Definition)
	ord_defs = nil
}

func AddVar(name string) *Definition {
	v := new(VarDef)
	v.name = name

	def := new(Definition)
	def.kind = DEF_Var
	def.d_var = v

	all_defs[name] = def
	return def
}

func AddFunc(name string) *Definition {
	fu := new(FuncDef)
	fu.name = name

	fu.locals  = make([]*LocalVar, 0)
	fu.loc_map = make(map[string]int)
	fu.labels  = make(map[string]bool)

	fu.inline_deps = make(map[*FuncDef]bool)
	fu.inline_seen = make(map[*FuncDef]bool)

	def := new(Definition)
	def.kind = DEF_Func
	def.d_func = fu

	all_defs[name] = def
	return def
}

func AddExtern(name string) *Definition {
	ext := new(ExternDef)
	ext.name = name

	def := new(Definition)
	def.kind = DEF_Extern
	def.d_extern = ext

	all_defs[name] = def
	return def
}

func CreateOrderedDefs() {
	ord_defs = make([]*Definition, 0, len(all_defs))

	for _, def := range all_defs {
		ord_defs = append(ord_defs, def)
	}

	sort.Slice(ord_defs, func(i, k int) bool {
		d1 := ord_defs[i]
		d2 := ord_defs[k]

		return d1.Name() < d2.Name()
	})
}

//----------------------------------------------------------------------

func (local *LocalVar) CalcCost(c_create, c_write, c_read int) {
	// compute the cost of keeping this local on the stack (as compared
	// to keeping it in a register).  the result is an estimation based
	// on the number of times the local is read and written, taking loops
	// into account.
	//
	// the `c_xxx` parameters are cost values, which should be >= 0.
	// -  c_create is the cost of creation
	// -  c_write  is the cost of each write (including the first)
	// -  c_read   is the cost of each read

	var cost int64

	if local.param_idx >= 0 {
		if local.abi_reg != REG_ON_STACK {
			// this parameter begins in a reg, but gets written to the stack.
			cost += int64(c_create)
		}
	} else {
		if local.writes > 0 {
			cost += int64(c_create)
		}
	}

	cost += int64(c_write) * int64(local.writes)
	cost += int64(c_read)  * int64(local.reads)

	// make temporary vars be slighly higher
	if local.name[0] == '.' {
		cost += 1
	}

	local.cost = cost
}

func (A *LocalVar) OrdLess(B *LocalVar) bool {
	// firstly, handle parameters
	if A.param_idx >= 0 {
		if B.param_idx < 0 {
			return true
		}
		return A.param_idx < B.param_idx
	}
	if B.param_idx >= 0 {
		return false
	}

	// secondly, look at live range
	if A.live_start != B.live_start {
		return A.live_start < B.live_start
	}
	if A.live_end != B.live_end {
		return A.live_end < B.live_end
	}

	// tie breaker, look at the name
	return A.name < B.name
}

//----------------------------------------------------------------------

type OperationInfo int
const (
	OP_UNARY  OperationInfo = (1 << 0)  // unary (no extra terms)
	OP_BINARY OperationInfo = (1 << 1)  // binary (one extra term)
	OP_CONV   OperationInfo = (1 << 2)  // conversion (needs dest type)
	OP_CMP    OperationInfo = (1 << 3)  // comparison (produces bool)
	OP_SHIFT  OperationInfo = (1 << 4)  // shift (count must be i8)

	OP_INT    OperationInfo = (1 << 6)  // works on integers
	OP_BOOL   OperationInfo = (1 << 7)  // works on booleans
	OP_FLOAT  OperationInfo = (1 << 8)  // works on floats
	OP_PTR    OperationInfo = (1 << 9)  // works on pointers
)

func FindOperation(name string) OperationInfo {
	// unary operations
	switch name {
		case "ineg":    return OP_UNARY | OP_INT
		case "iabs":    return OP_UNARY | OP_INT
		case "iflip":   return OP_UNARY | OP_INT
		case "iswap":   return OP_UNARY | OP_INT
		case "ibig":    return OP_UNARY | OP_INT
		case "ilittle": return OP_UNARY | OP_INT

		case "fneg":    return OP_UNARY | OP_FLOAT
		case "fabs":    return OP_UNARY | OP_FLOAT
		case "fsqrt":   return OP_UNARY | OP_FLOAT
		case "fround":  return OP_UNARY | OP_FLOAT
		case "ftrunc":  return OP_UNARY | OP_FLOAT
		case "ffloor":  return OP_UNARY | OP_FLOAT
		case "fceil":   return OP_UNARY | OP_FLOAT

		case "bnot":    return OP_UNARY | OP_BOOL

		case "izero?":  return OP_UNARY | OP_CMP | OP_INT
		case "isome?":  return OP_UNARY | OP_CMP | OP_INT
		case "ineg?":   return OP_UNARY | OP_CMP | OP_INT
		case "ipos?":   return OP_UNARY | OP_CMP | OP_INT
		case "fzero?":  return OP_UNARY | OP_CMP | OP_FLOAT
		case "fsome?":  return OP_UNARY | OP_CMP | OP_FLOAT
		case "fneg?":   return OP_UNARY | OP_CMP | OP_FLOAT
		case "fpos?":   return OP_UNARY | OP_CMP | OP_FLOAT
		case "fnan?":   return OP_UNARY | OP_CMP | OP_FLOAT
		case "finf?":   return OP_UNARY | OP_CMP | OP_FLOAT
		case "pnull?":  return OP_UNARY | OP_CMP | OP_PTR
		case "pref?":   return OP_UNARY | OP_CMP | OP_PTR
	}

	// binary operations
	switch name {
		case "iadd":   return OP_BINARY | OP_INT
		case "isub":   return OP_BINARY | OP_INT
		case "iand":   return OP_BINARY | OP_INT
		case "ior":    return OP_BINARY | OP_INT
		case "ixor":   return OP_BINARY | OP_INT
		case "imul":   return OP_BINARY | OP_INT
		case "idivt":  return OP_BINARY | OP_INT
		case "udivt":  return OP_BINARY | OP_INT
		case "iremt":  return OP_BINARY | OP_INT
		case "uremt":  return OP_BINARY | OP_INT
		case "imax":   return OP_BINARY | OP_INT
		case "imin":   return OP_BINARY | OP_INT
		case "umax":   return OP_BINARY | OP_INT
		case "umin":   return OP_BINARY | OP_INT

		case "ishl":   return OP_BINARY | OP_SHIFT | OP_INT
		case "ishr":   return OP_BINARY | OP_SHIFT | OP_INT
		case "ushl":   return OP_BINARY | OP_SHIFT | OP_INT
		case "ushr":   return OP_BINARY | OP_SHIFT | OP_INT

		case "fadd":   return OP_BINARY | OP_FLOAT
		case "fsub":   return OP_BINARY | OP_FLOAT
		case "fmul":   return OP_BINARY | OP_FLOAT
		case "fdiv":   return OP_BINARY | OP_FLOAT
		case "fremt":  return OP_BINARY | OP_FLOAT
		case "fmax":   return OP_BINARY | OP_FLOAT
		case "fmin":   return OP_BINARY | OP_FLOAT

		case "padd":   return OP_BINARY | OP_PTR
		case "psub":   return OP_BINARY | OP_PTR
		case "pdiff":  return OP_BINARY | OP_PTR
		case "pmin":   return OP_BINARY | OP_PTR
		case "pmax":   return OP_BINARY | OP_PTR

		case "band":   return OP_BINARY | OP_BOOL
		case "bor":    return OP_BINARY | OP_BOOL
		case "bxor":   return OP_BINARY | OP_BOOL
		case "bmax":   return OP_BINARY | OP_BOOL
		case "bmin":   return OP_BINARY | OP_BOOL

		case "ieq?":   return OP_BINARY | OP_CMP | OP_INT
		case "ine?":   return OP_BINARY | OP_CMP | OP_INT
		case "ilt?":   return OP_BINARY | OP_CMP | OP_INT
		case "ile?":   return OP_BINARY | OP_CMP | OP_INT
		case "igt?":   return OP_BINARY | OP_CMP | OP_INT
		case "ige?":   return OP_BINARY | OP_CMP | OP_INT

		case "ueq?":   return OP_BINARY | OP_CMP | OP_INT
		case "une?":   return OP_BINARY | OP_CMP | OP_INT
		case "ult?":   return OP_BINARY | OP_CMP | OP_INT
		case "ule?":   return OP_BINARY | OP_CMP | OP_INT
		case "ugt?":   return OP_BINARY | OP_CMP | OP_INT
		case "uge?":   return OP_BINARY | OP_CMP | OP_INT

		case "feq?":   return OP_BINARY | OP_CMP | OP_FLOAT
		case "fne?":   return OP_BINARY | OP_CMP | OP_FLOAT
		case "flt?":   return OP_BINARY | OP_CMP | OP_FLOAT
		case "fle?":   return OP_BINARY | OP_CMP | OP_FLOAT
		case "fgt?":   return OP_BINARY | OP_CMP | OP_FLOAT
		case "fge?":   return OP_BINARY | OP_CMP | OP_FLOAT

		case "peq?":   return OP_BINARY | OP_CMP | OP_PTR
		case "pne?":   return OP_BINARY | OP_CMP | OP_PTR
		case "plt?":   return OP_BINARY | OP_CMP | OP_PTR
		case "ple?":   return OP_BINARY | OP_CMP | OP_PTR
		case "pgt?":   return OP_BINARY | OP_CMP | OP_PTR
		case "pge?":   return OP_BINARY | OP_CMP | OP_PTR

		case "beq?":   return OP_BINARY | OP_CMP | OP_BOOL
		case "bne?":   return OP_BINARY | OP_CMP | OP_BOOL
		case "blt?":   return OP_BINARY | OP_CMP | OP_BOOL
		case "ble?":   return OP_BINARY | OP_CMP | OP_BOOL
		case "bgt?":   return OP_BINARY | OP_CMP | OP_BOOL
		case "bge?":   return OP_BINARY | OP_CMP | OP_BOOL
	}

	// conversion operations
	switch name {
		case "iext":   return OP_CONV | OP_INT
		case "uext":   return OP_CONV | OP_INT
		case "fext":   return OP_CONV | OP_FLOAT

		case "itof":   return OP_CONV | OP_INT
		case "utof":   return OP_CONV | OP_INT
		case "ftoi":   return OP_CONV | OP_FLOAT
		case "ftou":   return OP_CONV | OP_FLOAT
		case "btoi":   return OP_CONV | OP_BOOL

		// this works as several types (it is special)
		case "raw-cast": return OP_CONV
	}

	// an unknown operation
	return 0
}

func OperationType(info OperationInfo) BaseType {
	if (info & OP_INT)   != 0 { return TYP_Int     }
	if (info & OP_BOOL)  != 0 { return TYP_Bool    }
	if (info & OP_FLOAT) != 0 { return TYP_Float   }
	if (info & OP_PTR)   != 0 { return TYP_Pointer }

	return TYP_Void
}
