// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "io"
import "fmt"
import "sort"
import "path/filepath"

var OK     error
var FAILED error

type LocError struct {
	Str  string // error message
	What string // function/var/const being compiled, or ""
	File int    // file # where error occurred, or 0
	Line int    // line # where error occurred, or 0
}

// have_errors is set if any errors have been posted.
var have_errors bool

// _cur_error contains the current location of parsing (file, line, etc).
// this variable is private to this file!
var _cur_error LocError

// error_list contains all the errors posted via PostError().
var error_list []*LocError

//----------------------------------------------------------------------

func ClearErrors() {
	have_errors = false

	error_list = make([]*LocError, 0)

	_cur_error.Str  = ""
	_cur_error.What = ""
	_cur_error.File = 0
	_cur_error.Line = 0
}

func SortErrors() {
	sort.Slice(error_list, func(i, k int) bool {
		F1 := error_list[i].File
		F2 := error_list[k].File

		if F1 != F2 {
			return F1 < F2
		}

		L1 := error_list[i].Line
		L2 := error_list[k].Line

		if L1 == 0 { L1 = 99999999 }
		if L2 == 0 { L2 = 99999999 }

		return L1 < L2
	});
}

func ShowErrors(dest io.Writer) {
	SortErrors()

	for _, loc := range error_list {
		fmt.Fprintf(dest, "%s\n", loc.Format())
	}
}

func PostError(format string, a ...interface{}) {
	//panic(format)

	// copy the current location, set the message
	loc := new(LocError)
	*loc = _cur_error

	loc.Str = fmt.Sprintf(format, a...)

	error_list = append(error_list, loc)

	have_errors = true
}

func Error_SetFile(File int) {
	_cur_error.File = File
	_cur_error.Line = 0
	_cur_error.What = ""
}

func Error_SetLine(Line int) {
	_cur_error.Line = Line
}

func Error_SetPos(pos Position) {
	_cur_error.File = pos.file
	_cur_error.Line = pos.line
}

func Error_SetWhat(name string) {
	_cur_error.What = ""
}

//----------------------------------------------------------------------

func (loc *LocError) Format() string {
	if loc.File == 0 {
		// this is the REPL, no file or line number to show
		return loc.Str
	}

	// format error messages as GCC would do it
	f_part := filepath.Base(all_filenames[loc.File - 1])
	l_part := fmt.Sprintf("%d", loc.Line)

	return f_part + ":" + l_part + ": " + loc.Str
}
