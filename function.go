// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"

func (fu *FuncDef) Parse() {
	old_body := fu.body
	fu.body = NewNode(NG_Block, "", old_body.pos)

	for _, line := range old_body.children {
		fu.ParseLine(line)
	}

	fu.DetectMissingLabels()

	// ensure the body is never empty (for live ranges)
	if fu.body.Len() == 0 {
		cmt := NewNode(NH_Comment, "empty function", old_body.pos)
		fu.body.Add(cmt)
	}

//	Dumpty("new body:", fu.body, 3)
}

func (fu *FuncDef) ParseLine(line *Node) error {
	Error_SetPos(line.pos)

	fu.tok = line.children

	// the lexer ensures lines are never empty
	head := fu.tok[0]

	if head.kind == NH_Label {
		fu.tok = fu.tok[1:]
		return fu.Func_Label(head)
	}

	if fu.MatchWord("file")  { return fu.Func_File() }
	if fu.MatchWord("line")  { return fu.Func_Line() }
	if fu.MatchWord("jump")  { return fu.Func_Jump() }

	if fu.MatchWord("call")  { return fu.Func_Call() }
	if fu.MatchWord("ret")   { return fu.Func_Return(head) }
	if fu.MatchWord("swap")  { return fu.Func_Swap() }

	if fu.MatchWord("panic")       { return fu.Func_Panic() }
	if fu.MatchWord("check-null")  { return fu.Func_CheckNull() }
	if fu.MatchWord("check-range") { return fu.Func_CheckRange() }

	if head.kind == NL_Name && head.sigil == 0 {
		PostError("unknown statement: %s", head.str)
		return FAILED
	}

	// anything else should be an assignment
	return fu.Func_Assignment()
}

//----------------------------------------------------------------------

func (fu *FuncDef) Func_Label(t_label *Node) error {
	if len(fu.tok) > 0 {
		PostError("extra rubbish after label")
		return FAILED
	}

	if fu.HasLabel(t_label.str) {
		PostError("label .%s is already defined", t_label.str)
		return FAILED
	}

	// mark label as used
	fu.labels[t_label.str] = true

	fu.body.Add(t_label)
	return OK
}

func (fu *FuncDef) Func_File() error {
	if len(fu.tok) < 1 {
		PostError("missing string after file")
		return FAILED
	}
	if len(fu.tok) > 1 {
		PostError("extra rubbish at end of file statement")
		return FAILED
	}

	t_glob := fu.ParseTerm()
	if t_glob == P_FAIL {
		return FAILED
	}

	if t_glob.kind != NP_GlobPtr {
		PostError("expected global name after file")
		return FAILED
	}

	t_glob.kind = NH_File

	fu.body.Add(t_glob)
	return OK
}

func (fu *FuncDef) Func_Line() error {
	if len(fu.tok) < 1 {
		PostError("missing line number after line")
		return FAILED
	}
	if len(fu.tok) > 2 {
		PostError("extra rubbish at end of line statement")
		return FAILED
	}

	t_number := fu.tok[0]
	fu.tok    = fu.tok[1:]

	if t_number.kind != NL_Integer {
		PostError("expected line number after line")
		return FAILED
	}

	line_num, err := strconv.ParseInt(t_number.str, 0, 64)
	if err != nil || line_num < 0 {
		PostError("invalid line number")
		return FAILED
	}

	comment := ""

	if len(fu.tok) > 0 {
		t_string := fu.tok[0]

		if t_string.kind != NL_String {
			PostError("line comment must be a string")
			return FAILED
		}

		comment = t_string.str
	}

	t_cmt := NewNode(NH_Comment, comment, t_number.pos)
	t_cmt.offset = line_num

	fu.body.Add(t_cmt)
	return OK
}

func (fu *FuncDef) Func_Swap() error {
	t1 := fu.ParseTarget(false)
	if t1 == P_FAIL {
		return FAILED
	}

	t2 := fu.ParseTarget(false)
	if t2 == P_FAIL {
		return FAILED
	}

	// check the types...
	if ! TypeCompare(t1.ty, t2.ty) {
		PostError("type mismatch in swap statement")
		return FAILED
	}

	t_swap := NewNode(NH_Swap, "", t1.pos)
	t_swap.ty = t1.ty
	t_swap.Add(t1)
	t_swap.Add(t2)

	fu.body.Add(t_swap)
	return OK
}

func (fu *FuncDef) Func_Jump() error {
	if len(fu.tok) < 1 {
		PostError("missing label after jump")
		return FAILED
	}

	t_label := fu.tok[0]
	fu.tok   = fu.tok[1:]

	if ! (t_label.kind == NL_Name && t_label.sigil == 0) {
		PostError("expected label after jump, got: %s", t_label.String())
		return FAILED
	}

	t_jump := NewNode(NH_Jump, t_label.str, t_label.pos)

	if fu.MatchWord("if") {
		if fu.MatchWord("not") {
			t_jump.flags = t_jump.flags | NF_Not
		}

		comp := fu.ParseComputation()
		if comp == P_FAIL {
			return FAILED
		}

		if comp.ty.kind != TYP_Bool {
			PostError("jump condition must have boolean type")
			return FAILED
		}

		t_jump.Add(comp)
	}

	fu.body.Add(t_jump)
	return OK
}

func (fu *FuncDef) Func_Call() error {
	t_call := fu.Comp_FunCall(false)
	if t_call == P_FAIL {
		return FAILED
	}

	if (t_call.flags & NF_Tail) > 0 {
		// this limitation stems from the Win64 ABI, since tail-calls can
		// only pass parameters via registers (NEVER via the stack).
		if t_call.Len() - 1 > 4 {
			PostError("too many parameters for a tail call (limit is 4)")
			return FAILED
		}

		t_call.kind  = NC_TailCall
		t_call.flags = NF_NoReturn
	}

	t_drop := NewNode(NH_Drop, "", t_call.pos)
	t_drop.Add(t_call)

	fu.body.Add(t_drop)
	return OK
}

func (fu *FuncDef) Func_Return(head *Node) error {
	is_void := fu.ret_type.kind == TYP_Void

	t_ret := NewNode(NH_Return, "", head.pos)

	if len(fu.tok) > 0 {
		if is_void {
			PostError("ret with value in void function")
			return FAILED
		}

		comp := fu.ParseComputation()
		if comp == P_FAIL {
			return FAILED
		}

		if ! TypeCompare(comp.ty, fu.ret_type) {
			PostError("type mismatch with return value")
			return FAILED
		}

		t_ret.Add(comp)

	} else {
		if ! is_void {
			PostError("ret missing value in non-void function")
			return FAILED
		}
	}

	fu.body.Add(t_ret)
	return OK
}

func (fu *FuncDef) Func_Panic() error {
	term := fu.ParseTerm()
	if term == P_FAIL {
		return FAILED
	}

	if len(fu.tok) > 0 {
		PostError("extra rubbish at end of panic statement")
		return FAILED
	}

	if term.ty.kind != TYP_Pointer {
		PostError("panic requires a pointer type (to a string)")
		return FAILED
	}

	t_panic := NewNode(NH_Panic, "", term.pos)
	t_panic.Add(term)

	fu.body.Add(t_panic)
	return OK
}

func (fu *FuncDef) Func_CheckNull() error {
	term := fu.ParseTerm()
	if term == P_FAIL {
		return FAILED
	}

	if len(fu.tok) > 0 {
		PostError("extra rubbish at end of check-null statement")
		return FAILED
	}

	// the address of a global is never null
	if term.kind == NP_GlobPtr {
		return OK
	}

	if term.ty.kind != TYP_Pointer {
		PostError("check-null requires a pointer type")
		return FAILED
	}

	t_check := NewNode(NH_ChkNull, "", term.pos)
	t_check.Add(term)

	fu.body.Add(t_check)
	return OK
}

func (fu *FuncDef) Func_CheckRange() error {
	if len(fu.tok) < 2 {
		PostError("missing size or operand to check-range")
		return FAILED
	}

	t_size := fu.tok[0]
	fu.tok  = fu.tok[1:]

	if t_size.kind != NL_Integer {
		PostError("expected integer after check-range")
		return FAILED
	}

	size, err := strconv.ParseInt(t_size.str, 0, 64)
	if err != nil || size < 1 {
		PostError("invalid size in check-range")
		return FAILED
	}

	term := fu.ParseTerm()
	if term == P_FAIL {
		return FAILED
	}

	// note that we allow ANY size of integer here
	if term.ty.kind != TYP_Int {
		PostError("check-range requires an integer type")
		return FAILED
	}

	t_check := NewNode(NH_ChkRange, "", term.pos)
	t_check.offset = size
	t_check.Add(term)

	fu.body.Add(t_check)
	return OK
}

func (fu *FuncDef) Func_Assignment() error {
	target := fu.ParseTarget(true)
	if target == P_FAIL {
		return FAILED
	}

	if ! fu.MatchWord("=") {
		PostError("missing '=' in assignment")
		return FAILED
	}

	comp := fu.ParseComputation()
	if comp == P_FAIL {
		return FAILED
	}

	// handle new locals
	if target.kind == NP_Local && target.ty == nil {
		target.ty = comp.ty
		target.local.ty = comp.ty
	}

	if target.kind == NP_Memory {
		target.ty = comp.ty
	}

	// check types...
	if ! TypeCompare(target.ty, comp.ty) {
		PostError("type mismatch: target is %s, value is %s", target.ty.String(), comp.ty.String())
		return FAILED
	}

	t_move := NewNode(NH_Move, "", target.pos)
	t_move.ty = comp.ty
	t_move.Add(target)
	t_move.Add(comp)

	fu.body.Add(t_move)
	return OK
}

//----------------------------------------------------------------------

func (fu *FuncDef) DetectMissingLabels() error {
	for _, t := range fu.body.children {
		if t.kind == NH_Jump {
			if ! fu.HasLabel(t.str) {
				Error_SetPos(t.pos)
				PostError("no such label: .%s", t.str)
				return FAILED
			}
		}
	}

	return OK
}

func (fu *FuncDef) RemoveUnusedLabels() {
	fu.labels = make(map[string]bool)

	for _, t := range fu.body.children {
		if t.kind == NH_Jump {
			fu.labels[t.str] = true
		}
	}

	new_body := NewNode(NG_Block, "", fu.body.pos)

	for _, t := range fu.body.children {
		if t.kind == NH_Label {
			if ! fu.labels[t.str] {
				continue
			}
		}
		new_body.Add(t)
	}

	fu.body = new_body
}

//----------------------------------------------------------------------

func (fu *FuncDef) MatchWord(name string) bool {
	if len(fu.tok) == 0 {
		return false
	}
	if fu.tok[0].Match(name) {
		fu.tok = fu.tok[1:]
		return true
	}
	return false
}

func (fu *FuncDef) AddParameter(name string, ty *Type) {
	local := fu.NewLocal(name, ty)
	local.param_idx = len(fu.locals)

	fu.params    = fu.params + 1
	fu.par_types = append(fu.par_types, ty)
}

func (fu *FuncDef) NewLocal(name string, ty *Type) *LocalVar {
	local := new(LocalVar)
	local.name = name
	local.ty   = ty
	local.param_idx = -1

	fu.locals = append(fu.locals, local)
	fu.loc_map[name] = len(fu.locals)

	return local
}

func (fu *FuncDef) FindLocal(name string) *LocalVar {
	idx := fu.loc_map[name]
	if idx > 0 {
		return fu.locals[idx - 1]
	} else {
		return nil
	}
}

func (fu *FuncDef) HasLabel(name string) bool {
	return fu.labels[name]
}

func (fu *FuncDef) FindLabel(label string) int {
	for idx, line := range fu.body.children {
		if line.kind == NH_Label && line.str == label {
			return idx
		}
	}

	panic("no such label: " + label)
}
