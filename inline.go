// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strings"
import "strconv"

const INLINE_SUFFIX = '~'

func InlineFunctions() {
	for InlineNextFunction() {
		// keep going until all done
	}
}

func InlineNextFunction() bool {
	// find a function to perform inlining on.
	// we prefer one which does not inline any functions
	// which themselves are inlining somthing.
	// returns FALSE if no function needs to be handled.

	var best *FuncDef
	var best_rank int = (1 << 30)

	for _, def := range all_defs {
		if def.kind != DEF_Func {
			continue
		}

		fu := def.d_func
		if len(fu.inline_deps) == 0 {
			// nothing to inline
			continue
		}

		rank := fu.CalcRank()

		if rank < best_rank {
			best = fu
			best_rank = rank
		} else if fu.name < best.name {
			best = fu
		}
	}

	if best != nil {
		PerformInlining(best)
		// Dumpty("NEW CODE:", best.body, 3)
		return true
	}

	return false
}

func (fu *FuncDef) CalcRank() int {
	rank := 0
	for dep, _ := range fu.inline_deps {
		if len(dep.inline_deps) > 0 {
			rank = rank + 1
		}
	}
	return rank
}

//----------------------------------------------------------------------

type InlineData struct {
	// the function being inlined
	other *FuncDef

	// position of calling node
	pos Position

	// mapping from other local --> new local
	local_map  map[*LocalVar]*LocalVar

	// mapping from other label --> new label
	label_map  map[string]string

	// local used to store result, or NIL
	res_local  *LocalVar

	// label used to simulate the "ret" statement
	end_label  string

	// the new body for the current function
	body *Node
}

func NewInlineData() *InlineData {
	dat := new(InlineData)
	dat.local_map = make(map[*LocalVar]*LocalVar)
	dat.label_map = make(map[string]string)
	return dat
}

func (dat *InlineData) MapLocal(local *LocalVar) *LocalVar {
	result, exist := dat.local_map[local]
	if ! exist {
		panic("unmapped local")
	}
	return result
}

func (dat *InlineData) MapLabel(label string) string {
	result, exist := dat.label_map[label]
	if ! exist {
		panic("unmapped label")
	}
	return result
}

//----------------------------------------------------------------------

func PerformInlining(fu *FuncDef) {
	for len(fu.inline_deps) > 0 {
		PerformInlinePass(fu)
	}
}

func PerformInlinePass(fu *FuncDef) {
	// the dependecy info is recreated afterwards
	fu.inline_deps = make(map[*FuncDef]bool)

	// collect other functions inlined in this pass
	cur_seen := make(map[*FuncDef]bool)

	// since a function may inline itself, cannot rebuild body as we go
	new_body := NewNode(NG_Block, "", fu.body.pos)

	for _, t := range fu.body.children {
		comp := GetComputation(t)

		if comp != nil {
			if (comp.flags & NF_Inline) > 0 {
				fu.Inline_Call(t, new_body, cur_seen)
				continue
			}
		}

		new_body.Add(t)
	}

	fu.body = new_body

	for other, _ := range cur_seen {
		fu.inline_seen[other] = true
	}

	fu.Inline_UpdateDeps()
}

func (fu *FuncDef) Inline_Call(t *Node, body *Node, cur_seen map[*FuncDef]bool) {
	t_call   := GetComputation(t)
	t_func := t_call.children[0]
	other  := t_func.def.d_func

	// if we have inlined this function in a previous pass, it means we
	// have cycles in the dependency chain, hence stop inlining now.
	if fu.inline_seen[other] {
		t_call.flags = t_call.flags & (^ NF_Inline)
		body.Add(t)
		return
	}

	cur_seen[other] = true

	dat := NewInlineData()
	dat.pos   = t_call.pos
	dat.other = other
	dat.body  = body

	// copy the locals and labels, including parameters, and create
	// mapping tables for them.
	fu.Inline_CopyLocals(dat)
	fu.Inline_CopyLabels(dat)

	// create assignments to the parameter locals
	fu.Inline_WriteParams(t_call, dat)

	// if the other function returns something, then we need a local
	// variable to store it.
	dat.res_local = fu.Inline_ResultLocal(t, dat)

	// to simulate a "ret" statement, we need a label
	dat.end_label = fu.FreshLabelName("__ret")
	end_needed := false

	// copy the statements
	for _, t2 := range other.body.children {
		if t2.kind == NH_Return {
			end_needed = true
			fu.Inline_Return(t2, dat)
		} else {
			fu.Inline_Statement(t2, dat)
		}
	}

	if end_needed {
		t_label := NewNode(NH_Label, dat.end_label, t.pos)
		body.Add(t_label)
	}

	// finally, transform the node doing the call.
	// e.g. for a NH_Move, replace NC_Call with res_local.
	fu.Inline_TransformCall(t, dat)
}

func (fu *FuncDef) Inline_CopyLocals(dat *InlineData) {
	// since we can have other == fu, need to collect a separate list
	list := make([]*LocalVar, 0)

	for _, local := range dat.other.locals {
		list = append(list, local)
	}

	for _, local := range list {
		new_name := fu.FreshLocalName(local.name)
		new_loc  := fu.NewLocal(new_name, local.ty)

		dat.local_map[local] = new_loc
	}
}

func (fu *FuncDef) Inline_CopyLabels(dat *InlineData) {
	list := make([]string, 0)

	for label, _ := range dat.other.labels {
		list = append(list, label)
	}

	for _, label := range list {
		new_name := fu.FreshLabelName(label)
		fu.labels[new_name] = true

		dat.label_map[label] = new_name
	}
}

func (fu *FuncDef) Inline_WriteParams(t_call *Node, dat *InlineData) {
	for i := 0; i < dat.other.params; i = i + 1 {
		t_par := t_call.children[1 + i]

		new_local := dat.MapLocal(dat.other.locals[i])

		var_dest := NewNode(NP_Local, "", t_call.pos)
		var_dest.ty    = new_local.ty
		var_dest.local = new_local

		t_move := NewNode(NH_Move, "", t_call.pos)
		t_move.Add(var_dest)
		t_move.Add(t_par)

		dat.body.Add(t_move)
	}
}

func (fu *FuncDef) Inline_ResultLocal(t *Node, dat *InlineData) *LocalVar {
	if dat.other.ret_type.kind == TYP_Void {
		return nil
	}

	if t.kind == NH_Drop {
		// no local needed, result is being ignored
		return nil
	}

	if t.kind == NH_Move {
		t_dest := t.children[0]
		if t_dest.kind == NP_Local {
			// use the existing local
			return t_dest.local
		}
	}

	res_name  := fu.FreshLocalName("__inline")
	res_local := fu.NewLocal(res_name, dat.other.ret_type)

	return res_local
}

func (fu *FuncDef) Inline_Statement(t2 *Node, dat *InlineData) {
	new_node := fu.Inline_CopyNode(t2, dat)
	dat.body.Add(new_node)
}

func (fu *FuncDef) Inline_Return(t2 *Node, dat *InlineData) {
	// we simulate the return by transferring the value to the
	// result local, then jumping to the end label.

	if t2.Len() > 0 {
		comp := t2.children[0]

		// we can ignore most computations if no value is needed,
		// but a function call MUST be kept....
		if dat.res_local == nil {
			if comp.kind == NC_Call {
				t_drop := NewNode(NH_Drop, "", dat.pos)
				t_drop.Add(comp)
				dat.body.Add(t_drop)
			}
			return
		}

		t_result := NewNode(NP_Local, "", dat.pos)
		t_result.ty    = dat.res_local.ty
		t_result.local = dat.res_local

		t_move := NewNode(NH_Move, "", dat.pos)
		t_move.ty = t_result.ty
		t_move.Add(t_result)
		t_move.Add(comp)
	}

	t_jump := NewNode(NH_Jump, dat.end_label, dat.pos)
	dat.body.Add(t_jump)
}

func (fu *FuncDef) Inline_CopyNode(t1 *Node, dat *InlineData) *Node {
	t2 := NewNode(t1.kind, t1.str, dat.pos)

	t2.ty     = t1.ty
	t2.sigil  = t1.sigil
	t2.flags  = t1.flags
	t2.offset = t1.offset
	t2.def    = t1.def

	// handle remapping of locals and labels
	switch t1.kind {
	case NP_Local:
		t2.local = dat.MapLocal(t1.local)

	case NH_Label, NH_Jump:
		t2.str = dat.MapLabel(t1.str)
	}

	// recursively handle child nodes
	for _, child := range t1.children {
		t2.Add(fu.Inline_CopyNode(child, dat))
	}
	return t2
}

func (fu *FuncDef) Inline_TransformCall(t *Node, dat *InlineData) {
	// if there is no result local, then nothing is needed
	if dat.res_local == nil {
		return
	}
	if t.kind == NH_Drop {
		return
	}

	// when the result local already existed, nothing is needed
	if t.kind == NH_Move {
		t_dest := t.children[0]
		if t_dest.local == dat.res_local {
			return
		}
	}

	t_result := NewNode(NP_Local, "", t.pos)
	t_result.ty    = dat.res_local.ty
	t_result.local = dat.res_local

	ReplaceComputation(t, t_result)
	dat.body.Add(t)
}

func (fu *FuncDef) Inline_UpdateDeps() {
	for _, t := range fu.body.children {
		comp := GetComputation(t)

		if comp != nil {
			if (comp.flags & NF_Inline) > 0 {
				t_func := comp.children[0]
				fu.inline_deps[t_func.def.d_func] = true
			}
		}
	}
}

func (fu *FuncDef) FreshLocalName(name string) string {
	// determine a new name which does no clash with any existing one
	offset := 1

	for {
		new_name := NewInlineName(name, offset)
		if fu.FindLocal(new_name) == nil {
			return new_name
		}
		offset = offset + 1
	}
}

func (fu *FuncDef) FreshLabelName(name string) string {
	offset := 1

	for {
		new_name := NewInlineName(name, offset)
		if ! fu.HasLabel(new_name) {
			return new_name
		}
		offset = offset + 1
	}
}

func NewInlineName(name string, offset int) string {
	// add a suffix to the name which includes a number.
	// if such a suffix is already there, modify it instead.
	// offset should begin at 1, and increase to find a free name.

	pos := strings.LastIndexByte(name, INLINE_SUFFIX)

	if pos >= 0 {
		num, err := strconv.ParseInt(name[pos+1:], 10, 32)
		if err == nil {
			if num < 1 { num = 1 }
			offset = offset + int(num)
			name = name[0:pos]
		}
	}

	num_str := strconv.Itoa(offset)
	return name + string(INLINE_SUFFIX) + num_str
}

//----------------------------------------------------------------------

func GetComputation(t *Node) *Node {
	switch t.kind {
	case NH_Move:
		return t.children[1]

	case NH_Drop, NH_Jump, NH_Return:
		if t.Len() > 0 {
			return t.children[0]
		}
	}

	return nil
}

func ReplaceComputation(t *Node, comp *Node) {
	switch t.kind {
	case NH_Move:
		t.children[1] = comp

	case NH_Drop, NH_Jump, NH_Return:
		t.children[0] = comp

	default:
		panic("ReplaceComputation in odd node")
	}
}
