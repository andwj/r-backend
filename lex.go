// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "io"
import "bufio"
import "fmt"
import "strconv"
import "unicode"
import "unicode/utf8"

// Node represents an element of a parsed line or file.
type Node struct {
	kind     NodeKind
	children []*Node
	sigil    rune
	str      string
	flags    NodeFlag
	pos      Position

	ty       *Type
	def      *Definition
	local    *LocalVar
	offset   int64
	loop     int
}

type NodeKind int

const (
	/* low level nodes */

	NL_ERROR NodeKind = iota    // str is the message
	NL_EOF
	NL_EOLN

	NL_Integer  // an int:   [-]DDDDDDD          or [-]0xHHHHHH
	NL_Float    // a float:  [-]DDD.DDD[e[+-]DD] or [-]0xHH.HHHp[+-]DDD
	NL_FltSpec  // a special floating-point constant e.g. "+INF"
	NL_Char     // a char literal in '', encoded as integer: DDDDD
	NL_String   // a string in "", encoded as UTF-8

	NL_Name     // an identifier, `sigil` may be set
	NL_Symbol   // a special symbol, one of: `()[]{}`

	/* grouping nodes */

	NG_Line     // a parsed line of tokens
	NG_Expr     // a group of elements in `()` parentheses
	NG_Access   // a group of elements in `[]` square brackets
	NG_Block    // a group of elements in `{}` curly brackets

	/* data */

	ND_Zeroes   // offset is number of bytes

	/* statements */

	NH_INVALID NodeKind = 200 + iota

	NH_File     // def is a global var containing the filename
	NH_Comment  // offset is line number, str is comment ("" for none)

	NH_Drop     // computation.          discards the result.
	NH_Move     // dest | computation.   dest is NP_Local or NP_Memory.
	NH_Swap     // op1  | op2.  operands always NP_Local or NP_Memory.

	NH_Label    // a label for jumps
	NH_Jump     // computation (optional).  str is label, NF_Not flag.
	NH_Return   // computation (optional).

	NH_Panic    // def is the global var for string
	NH_ChkNull  // child is operand (a pointer)
	NH_ChkRange // child is operand (array index), offset is # elems

	/* computations */

	NC_Call     // func | parameter operands...  NF_Inline, NF_NoReturn flags.
	NC_TailCall // func | parameter operands...
	NC_OpSeq    // operand | operations... (at least one)
	NC_Matches  // match operand | value operands...  NF_Not flag.
	NC_StackVar // `offset` is the size to allocate

	/* op-seq nodes */

	NCX_Operator  // child is operand (for binary ops).  str is name.

	/* operands */

	NP_Local    // `local` field is the LocalVar struct
	NP_GlobPtr  // `def` is definition.  produces ptr to var/func.

	NP_Memory   // base ptr (NP_Local or NP_GlobPtr).  offset is set.
)

type NodeFlag int

const (
	NF_Not       NodeFlag = (1 << 0)
	NF_Dead      NodeFlag = (1 << 1)
	NF_Inline    NodeFlag = (1 << 4)
	NF_NoReturn  NodeFlag = (1 << 5)
	NF_Tail      NodeFlag = (1 << 6)
)

type Position struct {
	line int  // 0 if unknown
	file int  // 0 if unknown, index into all_filenames (+ 1)
}

type Lexer struct {
	reader *bufio.Reader
	pos    Position

	tokens  *Node   // tokens for current line, NIL if none yet
	pending *Node   // a pending error from SkipAhead

	hit_eof  bool   // we have reached the end-of-file
}

// NewLexer creates a Lexer from a Reader (e.g. a file).
func NewLexer(r io.Reader, file int) *Lexer {
	lexer := new(Lexer)

	lexer.reader   = bufio.NewReader(r)
	lexer.pos.file = file
	lexer.hit_eof  = false

	return lexer
}

// Scan reads and parses the current file and returns the next high-level
// token, usually a NG_Line.  It returns a NL_ERROR if there was a parsing
// error, or NL_EOF when the end of the file is reached.
func (lex *Lexer) Scan() *Node {
	if lex.pending != nil {
		res := lex.pending
		lex.pending = nil
		return res
	}
	if lex.hit_eof  {
		return NewNode(NL_EOF, "", lex.pos)
	}

	return lex.scanGroup(NG_Line, "", "")
}

// SkipAhead may be called after an error is returned from Scan(), and
// will reposition the token stream at the next top-level directive.
// If any error occurs, it will be the returned at next call to Scan().
func (lex *Lexer) SkipAhead() {
	lex.tokens  = nil
	lex.pending = nil

	for {
		t := lex.nextToken()

		if t.kind == NL_ERROR {
			lex.pending = t
			return
		}
		if t.kind == NL_EOF {
			return
		}

		s, err_tok := lex.fetchRawLine()
		if err_tok != nil {
			if err_tok.kind == NL_ERROR {
				lex.pending = err_tok
			}
			return
		}

		tok_list := lex.scanLine(s)
		if tok_list.kind == NL_ERROR {
			lex.pending = tok_list
			return
		}

		if tok_list.Len() > 0 {
			head := tok_list.children[0]

			if head.kind == NL_Name && LEX_CommonDirective(head.str) {
				// ensure nextToken will read this line
				lex.tokens = tok_list
				return
			}
		}
	}
}

func (lex *Lexer) scanGroup(kind NodeKind, opener, closer string) *Node {
	group := NewNode(kind, "", lex.pos)

	for {
		tok := lex.nextToken()

		if tok.kind == NL_ERROR {
			return tok
		}

		if tok.kind == NL_EOF {
			if opener != "" {
				// use the starting line for this error message
				msg := "unterminated " + opener + closer + " group"
				return NewNode(NL_ERROR, msg, group.pos)
			}
			return tok
		}

		if tok.kind == NL_EOLN {
			// handle '\' symbol to extend lines
			if group.Len() > 0 {
				last := group.children[group.Len() - 1]
				if last.Match("\\") {
					group.PopTail()
					continue
				}
			}

			// ignore EOLN inside a group
			if kind != NG_Line {
				continue
			}

			// skip empty lines
			if group.Len() == 0 {
				continue
			}

			return group
		}

		// found terminator of a group?
		if closer != "" {
			if tok.Match(closer) {
				return group
			}
		}

		// detect a stray closing parenthesis or bracket
		if tok.Match(")") || tok.Match("]") || tok.Match("}") {
			msg := "stray " + tok.str + " found"
			return NewNode(NL_ERROR, msg, group.pos)
		}

		// handle an opening parenthesis or bracket
		switch {
		case tok.Match("("): tok = lex.scanGroup(NG_Expr,   "(", ")")
		case tok.Match("["): tok = lex.scanGroup(NG_Access, "[", "]")
		case tok.Match("{"): tok = lex.scanGroup(NG_Block,  "{", "}")
		}

		// check for misuse of the '\' symbol
		if group.Len() > 0 {
			last := group.children[group.Len() - 1]
			if last.Match("\\") {
				return NewNode(NL_ERROR, "can only use \\ at end of a line", last.pos)
			}
		}

		// ensure non-empty lines begin at first token
		if kind == NG_Line {
			if group.Len() == 0 {
				group.pos = tok.pos
			}
		}

		group.Add(tok)
	}
}

// nextToken parses the next low-level token from the file.
// Result can be NL_ERROR for a parsing problem, or NL_EOF at the
// end of the file, or NL_EOLN for the end of a line.
func (lex *Lexer) nextToken() *Node {
	for {
		if lex.tokens != nil {
			if lex.tokens.Len() > 0 {
				return lex.tokens.PopHead()
			}
			lex.tokens = nil
		}

		s, err_tok := lex.fetchRawLine()
		if err_tok != nil {
			return err_tok
		}

		tok_list := lex.scanLine(s)
		if tok_list.kind == NL_ERROR {
			return tok_list
		}
		lex.tokens = tok_list
	}
}

func (lex *Lexer) fetchRawLine() (string, *Node) {
	// once stream is finished, keep returning EOF
	// [ this simplifies some logic in the calling code ]
	if lex.hit_eof  {
		return "", NewNode(NL_EOF, "", lex.pos)
	}

	lex.pos.line += 1

	// NOTE: this can return some data + io.EOF
	s, err := lex.reader.ReadString('\n')

	if err == io.EOF {
		lex.hit_eof = true
	} else if err != nil {
		lex.hit_eof = true
		return "", NewNode(NL_ERROR, err.Error(), lex.pos)
	}

	// strip CR and LF from the end
	if len(s) > 0 && s[len(s)-1] == '\n' {
		s = s[0:len(s)-1]
	}
	if len(s) > 0 && s[len(s)-1] == '\r' {
		s = s[0:len(s)-1]
	}

	return s, nil
}

//----------------------------------------------------------------------

// scanLine parses a single a line and produces a sequence of low-level
// tokens, returning them in a NG_Line token.  If there was a problem,
// like an unterminated string, then an NL_ERROR token is returned.
// Multi-line comments are handled here, via some state in the Lexer
// struct, but parentheses and brackets are not.
func (lex *Lexer) scanLine(s string) *Node {
	group := NewNode(NG_Line, "", lex.pos)

	// convert to runes
	r := []rune(s)

	// check for UTF-8 problems
	for _, ch := range r {
		if ch == utf8.RuneError {
			return NewNode(NL_ERROR, "bad UTF-8 found", group.pos)
		}
	}

	// skip initial whitespace
	for len(r) > 0 {
		ch := r[0]
		if unicode.Is(unicode.White_Space, ch) || unicode.IsControl(ch) {
			r = r[1:]
		} else {
			break
		}
	}

	// handle normal tokens
	for len(r) > 0 {
		ch := r[0]

		// whitespace ?
		if unicode.Is(unicode.White_Space, ch) || unicode.IsControl(ch) {
			r = r[1:]
			continue
		}

		// line comment ?
		if ch == ';' {
			break
		}

		// string ?
		if r[0] == '"' {
			size, tok := lex.scanString(r)
			tok.pos = group.pos
			if tok.kind == NL_ERROR {
				return tok
			}
			r = r[size:]
			group.Add(tok)
			continue
		}

		// character literal ?
		if r[0] == '\'' {
			size, tok := lex.scanCharacter(r)
			tok.pos = group.pos
			if tok.kind == NL_ERROR {
				return tok
			}
			r = r[size:]
			group.Add(tok)
			continue
		}

		// number ?
		if unicode.IsDigit(r[0]) ||
			(len(r) >= 2 && (r[0] == '-' || r[0] == '+') && unicode.IsDigit(r[1])) {

			size, tok := lex.scanNumber(r)
			tok.pos = group.pos
			if tok.kind == NL_ERROR {
				return tok
			}
			r = r[size:]
			group.Add(tok)
			continue
		}

		// handle non-ident symbols
		if LEX_SymbolChar(r[0]) {
			tok := NewNode(NL_Symbol, string(r[0:1]), lex.pos)
			r = r[1:]
			group.Add(tok)
			continue
		}

		// anything else MUST be a name or symbol
		if LEX_IdentifierChar(r[0]) {
			size, tok := lex.scanIdentifier(r)
			tok.pos = group.pos
			if tok.kind == NL_ERROR {
				return tok
			}
			r = r[size:]
			group.Add(tok)
			continue
		}

		msg := fmt.Sprintf("stray U+%04X in code", int(r[0]))
		return  NewNode(NL_ERROR, msg, lex.pos)
	}

	group.Add(NewNode(NL_EOLN, "", lex.pos))

	return group
}

// returns # of runes consumed, or negative on error
func (lex *Lexer) scanIdentifier(r []rune) (int, *Node) {
	pos := 1
	is_label := false

	for pos < len(r) {
		if r[pos] == ':' && pos+1 == len(r) {
			is_label = true
			break
		}
		if ! LEX_IdentifierChar(r[pos]) {
			break
		}
		pos += 1
	}

	// does identifier begin with a sigil?
	sigil := rune(0)
	start := 0

	if ! is_label {
		switch r[0] {
		case '@', '%':
			sigil = r[0]
			start = 1
		}
	}

	if pos == start {
		return -1, NewNode(NL_ERROR, "identifier too short", lex.pos)
	}

	s := string(r[start:pos])

	// a special constant?
	switch s {
	case "+INF", "-INF", "SNAN", "QNAN":
		return pos, NewNode(NL_FltSpec, s, lex.pos)
	}

	nd := NewNode(NL_Name, s, lex.pos)
	nd.sigil = sigil

	if is_label {
		nd.kind = NH_Label
		pos = pos + 1  // skip the ':'
	}
	return pos, nd
}

func (lex *Lexer) scanString(r []rune) (int, *Node) {
	// skip leading quote
	pos := 1

	s := ""

	for {
		if pos >= len(r) {
			return -1, NewNode(NL_ERROR, "unterminated string", lex.pos)
		}

		if r[pos] == '"' {
			break
		}

		// handle escapes
		if r[pos] == '\\' && pos+1 < len(r) {
			pos += 1

			esc, step := lex.scanEscape(r[pos:])
			if step == -2 {
				return -1, NewNode(NL_ERROR, "unknown escape in string", lex.pos)
			} else if step < 0 {
				return -1, NewNode(NL_ERROR, "malformed escape in string", lex.pos)
			}

			s = s + esc
			pos += step
			continue
		}

		s = s + string(r[pos])
		pos += 1
	}

	return pos + 1, NewNode(NL_String, s, lex.pos)
}

func (lex *Lexer) scanCharacter(r []rune) (int, *Node) {
	// skip leading quote
	pos := 1

	if len(r) < 3 || r[1] == '\'' {
		return -1, NewNode(NL_ERROR, "bad character literal", lex.pos)
	}

	if r[2] == '\'' && r[1] != '\\' {
		int_val := strconv.Itoa(int(r[1]))
		return 3, NewNode(NL_Char, int_val, lex.pos)
	}

	// handle escapes
	if r[pos] != '\\' {
		return -1, NewNode(NL_ERROR, "bad character literal", lex.pos)
	}

	pos += 1
	r = r[pos:]

	esc, step := lex.scanEscape(r)

	// need to convert decoded escape to a single rune
	var rch []rune
	if step >= 0 {
		rch = []rune(esc)
	}

	if step == -2 {
		return -1, NewNode(NL_ERROR, "unknown escape in char literal", lex.pos)
	} else if step < 0 {
		return -1, NewNode(NL_ERROR, "malformed escape in char literal", lex.pos)
	} else if !utf8.ValidString(esc) || len(rch) != 1 {
		return -1, NewNode(NL_ERROR, "char literal is not a valid unicode char", lex.pos)
	}

	r = r[step:]

	if len(r) < 1 || r[0] != '\'' {
		return -1, NewNode(NL_ERROR, "unterminated char literal", lex.pos)
	}

	step += pos + 1

	int_val := strconv.Itoa(int(rch[0]))

	return step, NewNode(NL_Char, int_val, lex.pos)
}

func (lex *Lexer) scanEscape(r []rune) (string, int) {
	switch r[0] {
	case '"', '`', '\'', '\\':
		return string(r[0]), 1
	case 'a':
		return "\u0007", 1   // bell
	case 'b':
		return "\u0008", 1   // backspace
	case 't':
		return "\u0009", 1   // horizontal tab
	case 'n':
		return "\u000A", 1  // linefeed
	case 'v':
		return "\u000B", 1  // vertical tab
	case 'f':
		return "\u000C", 1  // formfeed
	case 'r':
		return "\u000D", 1  // carriage return
	case 'e':
		return "\u001B", 1  // escape
	}

	// octal?
	// (requires one to three octal digits, as per C99 and NASM)
	if '0' <= r[0] && r[0] <= '7' {
		return lex.scanOctalEscape(r)
	}

	// hexadecimal?
	// (requires one or two hexadecimal digits, as per C99 and NASM)
	if r[0] == 'x' {
		return lex.scanHexEscape(r)
	}

	// unicode?
	// (these follow conventions of C11, Go and NASM)
	if r[0] == 'u' {
		return lex.scanUnicodeEscape(r, 4)
	}
	if r[0] == 'U' {
		return lex.scanUnicodeEscape(r, 8)
	}

	return "", -2
}

func (lex *Lexer) scanOctalEscape(r []rune) (string, int) {
	value := int(r[0] - '0')
	size  := 1

	if len(r) >= 2 && '0' <= r[1] && r[1] <= '7' {
		value = (value << 3) + int(r[1] - '0')
		size += 1
	}
	if len(r) >= 3 && '0' <= r[2] && r[2] <= '7' {
		value = (value << 3) + int(r[2] - '0')
		size += 1
	}

	if value > 255 {
		return "", -1
	}

	// convert to a string.
	// [ it may be invalid UTF-8 -- that is checked later ]
	var s [1]byte
	s[0] = byte(value)
	return string(s[:]), size
}

func (lex *Lexer) scanHexEscape(r []rune) (string, int) {
	value := 0
	size  := 1  // the 'x'

	for size < 3 && size < len(r) {
		ch := unicode.ToUpper(r[size])
		if '0' <= ch && ch <= '9' {
			value = value << 4
			value |= int(ch - '0')
		} else if 'A' <= ch && ch <= 'F' {
			value = value << 4
			value |= 10 + int(ch - 'A')
		} else {
			break
		}
		size += 1
	}

	if size < 2 {
		return "", -1  // no digits!
	}

	// convert to a string.
	// [ it may be invalid UTF-8 -- that is checked later ]
	var s [1]byte
	s[0] = byte(value)
	return string(s[:]), size
}

func (lex *Lexer) scanUnicodeEscape(r []rune, size int) (string, int) {
	// we assume here the first character is 'u' or 'U'

	if len(r) < size+1 {
		return "", -1
	}

	var value rune = 0

	for i := 1; i <= size; i++ {
		value = value << 4
		ch := unicode.ToUpper(r[i])
		if '0' <= ch && ch <= '9' {
			value |= (ch - '0')
		} else if 'A' <= ch && ch <= 'F' {
			value |= 10 + (ch - 'A')
		} else {
			return "", -1
		}
	}

	if value > 0x10FFFF {
		return "", -1
	}

	// convert to a string
	return string(value), size+1
}

// returns a token and # of runes consumed
func (lex *Lexer) scanNumber(r []rune) (int, *Node) {
	if len(r) >= 2 && r[0] == '0' && r[1] == 'b' {
		return lex.scanBinaryNumber(r, 2)
	}
	if len(r) >= 3 && (r[0] == '-' || r[0] == '+') && r[1] == '0' && r[2] == 'b' {
		return lex.scanBinaryNumber(r, 3)
	}

	if len(r) >= 2 && r[0] == '0' && r[1] == 'x' {
		return lex.scanHexNumber(r, 2)
	}
	if len(r) >= 3 && (r[0] == '-' || r[0] == '+') && r[1] == '0' && r[2] == 'x' {
		return lex.scanHexNumber(r, 3)
	}

	return lex.scanDecimalNumber(r, 0)
}

func (lex *Lexer) scanBinaryNumber(r []rune, pos int) (int, *Node) {
	is_neg := (r[0] == '-')

	s := ""

	for pos < len(r) {
		ch := r[pos]
		if ch == '0' || ch == '1' {
			s += string(ch)
			pos += 1
			continue
		}
		if unicode.IsDigit(ch) || unicode.IsLetter(ch) ||
			ch == '-' || ch == '+' || ch == '.' {

			msg := "illegal binary digit '" + string(ch) + "'"
			return -1, NewNode(NL_ERROR, msg, lex.pos)
		}
		// all other symbols terminate the number
		break
	}

	if s == "" {
		return -1, NewNode(NL_ERROR, "bad binary number (no digits)", lex.pos)
	}
	val, err := strconv.ParseUint(s, 2, 64)
	if err != nil {
		msg := fmt.Sprintf("bad binary number (too large)")
		return -1, NewNode(NL_ERROR, msg, lex.pos)
	}

	// convert to hexadecimal
	hex := "0x" + strconv.FormatUint(val, 16)
	if is_neg {
		hex = "-" + hex
	}
	return pos, NewNode(NL_Integer, hex, lex.pos)
}

func (lex *Lexer) scanDecimalNumber(r []rune, pos int) (int, *Node) {
	is_neg := (r[0] == '-')

	if (r[0] == '-' || r[0] == '+') {
		pos = 1
	}

	has_digit := false
	has_dot   := false
	has_E     := false
	has_exp   := false

	s := ""

	for pos < len(r) {
		ch := r[pos]

		if unicode.IsDigit(ch) {
			if has_E {
				has_exp = true
			} else {
				has_digit = true
			}
			s += string(ch)
			pos += 1
			continue
		}

		if ch == '.' {
			if !has_digit || has_dot || has_E {
				return -1, NewNode(NL_ERROR, "bad float (misplaced '.')", lex.pos)
			}
			has_dot = true
			s += string(ch)
			pos += 1
			continue
		}

		if ch == 'e' || ch == 'E' {
			if !has_digit || has_E {
				return -1, NewNode(NL_ERROR, "bad float (misplaced 'e')", lex.pos)
			}
			has_E = true
			s += string(ch)
			pos += 1

			if pos < len(r) && (r[pos] == '-' || r[pos] == '+') {
				s += string(r[pos])
				pos += 1
			}
			continue
		}

		if unicode.IsLetter(ch) || ch == '-' || ch == '+' {
			msg := "illegal decimal digit '" + string(ch) + "'"
			return -1, NewNode(NL_ERROR, msg, lex.pos)
		}

		// all other symbols terminate the number
		break
	}

	if s == "" {
		return -1, NewNode(NL_ERROR, "bad number (no digits)", lex.pos)
	}
	if has_E && !has_exp {
		return -1, NewNode(NL_ERROR, "bad float (missing exponent)", lex.pos)
	}

	if is_neg {
		s = "-" + s
	}

	kind := NL_Integer
	if has_dot || has_exp {
		kind = NL_Float
	}
	return pos, NewNode(kind, s, lex.pos)
}

func (lex *Lexer) scanHexNumber(r []rune, pos int) (int, *Node) {
	is_neg := (r[0] == '-')

	has_digit := false
	has_dot   := false
	has_P     := false
	has_exp   := false

	s := ""

	for pos < len(r) {
		ch := r[pos]

		if has_P && unicode.IsLetter(ch) {
			msg := "illegal hex exponent digit '" + string(ch) + "'"
			return -1, NewNode(NL_ERROR, msg, lex.pos)
		}

		if unicode.Is(unicode.Hex_Digit, ch) {
			if has_P {
				has_exp = true
			} else {
				has_digit = true
			}
			s += string(ch)
			pos += 1
			continue
		}

		if ch == '.' {
			if !has_digit || has_dot || has_P {
				return -1, NewNode(NL_ERROR, "bad hex float (misplaced '.')", lex.pos)
			}
			has_dot = true
			s += string(ch)
			pos += 1
			continue
		}

		if ch == 'p' || ch == 'P' {
			if !has_digit || has_P {
				return -1, NewNode(NL_ERROR, "bad hex float (misplaced 'p')", lex.pos)
			}
			has_P = true
			s += string(ch)
			pos += 1

			if pos < len(r) && (r[pos] == '-' || r[pos] == '+') {
				s += string(r[pos])
				pos += 1
			}
			continue
		}

		if unicode.IsLetter(ch) || ch == '-' || ch == '+' {
			msg := "illegal hex digit '" + string(ch) + "'"
			return -1, NewNode(NL_ERROR, msg, lex.pos)
		}

		// all other symbols terminate the number
		break
	}

	if s == "" {
		return -1, NewNode(NL_ERROR, "bad hex number (no digits)", lex.pos)
	}
	if (has_dot && !has_P) || (has_P && !has_exp) {
		return -1, NewNode(NL_ERROR, "bad hex float (missing exponent)", lex.pos)
	}

	s = "0x" + s
	if is_neg {
		s = "-" + s
	}

	kind := NL_Integer
	if has_dot || has_exp {
		kind = NL_Float
	}
	return pos, NewNode(kind, s, lex.pos)
}

func LEX_Whitespace(ch rune) bool {
	if unicode.Is(unicode.White_Space, ch) { return true }
	if unicode.IsControl(ch) { return true }
	return false
}

func LEX_SymbolChar(ch rune) bool {
	switch ch {
	case '(', ')', '[', ']', '{', '}': return true
	default: return false
	}
}

func LEX_IdentifierChar(ch rune) bool {
	if LEX_Whitespace(ch) { return false }
	if LEX_SymbolChar(ch) { return false }

	if ch == ';' { return false }

	return unicode.IsPrint(ch)
}

//----------------------------------------------------------------------

func NewNode(kind NodeKind, str string, pos Position) *Node {
	t := new(Node)
	t.kind = kind
	t.str  = str
	t.pos  = pos
	t.children = make([]*Node, 0)
	return t
}

func (t *Node) Len() int {
	return len(t.children)
}

func (t *Node) Last() *Node {
	if len(t.children) == 0 {
		return nil
	}
	return t.children[len(t.children)-1]
}

func (t *Node) Add(child *Node) {
	if child == nil {
		panic("Add with nil node")
	}
	t.children = append(t.children, child)
}

func (t *Node) AddFront(child *Node) {
	// resize the array
	t.children = append(t.children, nil)

	// shift elements up
	copy(t.children[1:], t.children[0:])

	t.children[0] = child
}

func (t *Node) PopHead() *Node {
	if t.Len() == 0 {
		panic("PopHead with no tokens")
	}
	head := t.children[0]
	t.children = t.children[1:]
	return head
}

func (t *Node) PopTail() *Node {
	if t.Len() == 0 {
		panic("PopTail with no tokens")
	}
	num  := len(t.children)
	tail := t.children[num-1]
	t.children = t.children[0:num-1]
	return tail
}

func (t *Node) Remove(idx int) {
	if idx >= t.Len() {
		panic("Remove with bad index")
	}
	for k := idx+1; k < t.Len(); k++ {
		t.children[k-1] = t.children[k]
	}
	t.children = t.children[0:t.Len()-1]
}

func (t *Node) Match(name string) bool {
	if t.sigil != 0 {
		return false
	}
	if t.kind == NL_Name || t.kind == NL_Symbol {
		return t.str == name
	}
	return false
}

func (t *Node) Find(name string) int {
	for i, elem := range t.children {
		if elem.Match(name) {
			return i
		}
	}
	return -1
}

func (t *Node) IsLiteral() bool {
	switch t.kind {
		case NL_Integer, NL_Char, NL_Float, NL_FltSpec, NL_String:
			return true
	}
	return false
}

func LEX_CommonDirective(name string) bool {
	// this only used for SkipAhead.
	// some things deliberately missing, especially "end".

	switch name {
		case "fun", "inline-fun", "var", "rom-var": return true
		case "extern", "public", "private": return true
		default: return false
	}
}

// String returns something usable in error messages,
// especially ones of the form "expected xxx, got: yyy".
func (t *Node) String() string {
	switch t.kind {
	case NL_Integer: return "an integer"
	case NL_Char:    return "a char literal"
	case NL_Float:   return "a float value"
	case NL_FltSpec: return "a special float value"
	case NL_String:  return "a string"

	case NL_Name:
		if t.sigil == '@' { return "a global name" }
		if t.sigil == '%' { return "a local name"  }
		return "an identifier"

	case NL_Symbol: return "'" + t.str + "'"

	case NG_Line:   return "a raw line"
	case NG_Expr:   return "stuff in ()"
	case NG_Access: return "stuff in []"
	case NG_Block:  return "stuff in {}"

	case NH_Label:  return "a label name"
	default:        return "something odd"
	}
}

func (t *Node) FullString() string {
	if t == nil {
		return "nil"
	}

	s := t.str
	if t.sigil != 0 {
		s = string(t.sigil) + s
	}
	if len(s) > 50 {
		s = s[0:50] + "..."
	}
	s = fmt.Sprintf(" %q", s)

	len_str := " (" + strconv.Itoa(t.Len()) + " elem)"

	flag_str := ""
	if (t.flags & NF_Not)      > 0 { flag_str = flag_str + " NOT"    }
	if (t.flags & NF_Dead)     > 0 { flag_str = flag_str + " DEAD"   }
	if (t.flags & NF_Inline)   > 0 { flag_str = flag_str + " INLINE" }
	if (t.flags & NF_NoReturn) > 0 { flag_str = flag_str + " NORET"  }

	switch t.kind {
	/* low level nodes */

	case NL_ERROR:    return "ERROR" + s
	case NL_EOF:      return "EOF"
	case NL_EOLN:     return "EOLN"

	case NL_Integer:  return "NL_Integer" + s
	case NL_Float:    return "NL_Float"   + s
	case NL_FltSpec:  return "NL_FltSpec" + s
	case NL_Char:     return "NL_Char"    + s
	case NL_String:   return "NL_String"  + s

	case NL_Name:     return "NL_Name"   + s
	case NL_Symbol:   return "NL_Symbol" + s

	/* grouping nodes */

	case NG_Line:     return "NG_Line"   + len_str
	case NG_Expr:     return "NG_Expr"   + len_str
	case NG_Access:   return "NG_Access" + len_str
	case NG_Block:    return "NG_Block"  + len_str

	/* back-end nodes */

	case NH_File:     return "NH_File"
	case NH_Comment:  return "NH_Comment"

	case NH_Drop:     return "NH_Drop"
	case NH_Move:     return "NH_Move" + flag_str
	case NH_Swap:     return "NH_Swap" + flag_str

	case NH_Label:    return "NH_Label" + s
	case NH_Jump:     return "NH_Jump"   + flag_str + s
	case NH_Return:   return "NH_Return" + flag_str

	case NH_Panic:    return "NH_Panic" + s
	case NH_ChkNull:  return "NH_ChkNull"
	case NH_ChkRange: return "NH_ChkRange"

	/* computations */

	case NC_Call:     return "NC_Call"     + flag_str
	case NC_TailCall: return "NC_TailCall" + flag_str
	case NC_OpSeq:    return "NC_OpSeq"    + flag_str
	case NC_Matches:  return "NC_Matches"  + flag_str
	case NC_StackVar: return "NC_StackVar" + flag_str

	/* op-seq nodes */

	case NCX_Operator: return "NCX_Operator" + s

	/* operands */

	case NP_Local:    return "NP_Local"   + " %" + t.local.name
	case NP_GlobPtr:  return "NP_GlobPtr" + " @" + t.def.Name()
	case NP_Memory:   return "NP_Memory"

	default:
		return "??UNKNOWN??"
	}
}

func Dump(msg string, t *Node, level int) {
	if msg != "" {
		println(msg)
	}

	s := fmt.Sprintf("%*s%s", level, "", t.FullString())
	println(s)

	if t.children != nil {
		for _, child := range t.children {
			if child.kind != NH_Comment {
				Dump("", child, level + 3)
			}
		}
	}
}

func Dumpty(msg string, t *Node, level int) {
	if msg != "" {
		println(msg)
	}

	ty_str := "---"
	if t.ty != nil {
		ty_str = t.ty.String()
	}

	if t.offset != 0 {
		ty_str += fmt.Sprintf(" offset=%d", t.offset)
	}

	s := fmt.Sprintf("%*s%s : %s", level, "", t.FullString(), ty_str)
	println(s)

	if t.children != nil {
		for _, child := range t.children {
			if child.kind == NH_Comment {
				s = fmt.Sprintf("%*s; %s", level + 3, "", child.str)
				println(s)
			} else {
				Dumpty("", child, level + 3)
			}
		}
	}
}

func DumpLoops(msg string, t *Node, level int) {
	if msg != "" {
		println(msg)
	}
	for i, t := range t.children {
		info := "       "
		if level == 0 {
			info = fmt.Sprintf("%3d (%d)", i, t.loop)
		}

		s := fmt.Sprintf("%s %*s%s", info, level, "", t.FullString())

		if t.kind == NH_Comment {
			s = fmt.Sprintf("%s %*s; %s", info, level, "", t.str)
		}

		println(s)

		DumpLoops("", t, level + 3)
	}
}
