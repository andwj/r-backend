// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "os"
import "fmt"
import "strings"
import "runtime"
import "path/filepath"

import "gitlab.com/andwj/argv"

var Options struct {
	out_dir string

	// the architecture, such as "amd64"
	arch string

	// the operating system, such as "linux" or "windows"
	os string

	no_optim   bool
	all_errors bool
	help       bool
}

var all_filenames []string

// Architecture is an interface which encapsulates methods for
// generating code for a particular architecture and assembler.
type Architecture interface {
	// OutputExt provides the normal extension for the output
	// assembly file.  It should include the dot.
	OutputExt() string

	// Begin is called just after the output file of a module is
	// created, but before any actual compilation has been done.
	Begin()

	// Finish is called just before the output file is closed.
	Finish()

	// this returns true for big endian CPUs, false for little endian.
	BigEndian() bool

	// RegisterBits gives the number of bits in a general purpose register.
	RegisterBits() int

	// RegName queries the assembler name of a register.
	RegName(reg Register) string

	// Generate creates the output assembly for a definition.
	Generate(def *Definition)

	// this is called by the stack allocation code
	CallDepth(t_call *Node) int
}

var arch Architecture

// this indicates that a parsing function which normally returns a
// Node pointer has failed, and posted an error via PostError.
var P_FAIL *Node

//----------------------------------------------------------------------

func main() {
	InitErrConsts()
	ClearErrors()

	all_filenames = make([]string, 0)

	// this sets up the `arch` global var.
	// it will fatal error on invalid arguments.
	HandleArgs()

	ClearErrors()

	InitTypes()

	// visit all files, even if some produce an error
	for f_idx, filename := range all_filenames {
		ProcessFile(f_idx + 1, filename)
	}

	if have_errors {
		ShowErrors(os.Stderr)
		FatalError("")
	}

	os.Exit(0)
}

func InitErrConsts() {
	// some important global constants
	OK     = nil
	FAILED = fmt.Errorf("FAILED")
	P_FAIL = NewNode(NL_ERROR, "", Position{})
}

func HandleArgs() {
	// default architecture : try to match host
	switch runtime.GOARCH {
	case "amd64":
		Options.arch = runtime.GOARCH
	default:
		Options.arch = "amd64"
	}

	// default OS : try to match host
	switch runtime.GOOS {
	case "windows", "linux":
		Options.os = runtime.GOOS
	default:
		Options.os = "linux"
	}

	argv.Generic("o", "output",     &Options.out_dir, "file", "name of output file/dir")
	argv.Generic("t", "target",     &Options.os, "OS", "target OS (linux|windows)")
	argv.Generic("a", "arch",       &Options.arch, "arch", "CPU architecture (amd64)")
	argv.Enabler("e", "all-errors", &Options.all_errors, "remove limit on errors per function")
	argv.Enabler("N", "no-optim",   &Options.no_optim, "disable all optimizations")
	argv.Enabler("h", "help",       &Options.help, "display this help text")

	err := argv.Parse()
	if err != nil {
		FatalError("%s", err.Error())
	}

	// WISH: support more architectures
	switch Options.arch {
	case "amd64":
		arch = NewAMD64Arch()
	default:
		FatalError("unknown arch: %s", Options.arch)
	}

	switch Options.os {
	case "linux", "windows":
		// ok
	default:
		FatalError("unknown OS: %s", Options.os)
	}

	if Options.help {
		ShowUsage()
		os.Exit(0)
	}

	all_filenames = argv.Unparsed()

	if len(all_filenames) == 0 {
		FatalError("no input files")
	}
}

func ShowUsage() {
	fmt.Printf("Usage: sageleaf FILE.ll [OPTIONS]\n")

	fmt.Printf("\n")
	fmt.Printf("Available options:\n")

	argv.Display(os.Stdout)
}

func FatalError(format string, a ...interface{}) {
	// WISH: close and delete any output files

	if format != "" {
		format = "sageleaf: fatal: " + format + "\n"
		fmt.Fprintf(os.Stderr, format, a...)
	}
	os.Exit(1)
}

func FileHasExtension(fn, ext string) bool {
		fn = filepath.Ext(fn)
		fn = strings.ToLower(fn)

		if len(fn) > 0 && fn[0] == '.' {
				fn = fn[1:]
		}

		return (fn == ext)
}

func FileRemoveExtension(fn string) string {
	old_ext := filepath.Ext(fn)
	return strings.TrimSuffix(fn, old_ext)
}

func AddFilename(name string) int {
	// file numbers are (1 + index)
	all_filenames = append(all_filenames, name)
	return len(all_filenames)
}
