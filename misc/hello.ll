;
; Hello world
;

public

fun @main (-> i32)
	call @puts @message
	ret i32 0
end

var rom @message =
	i8 "Hello World!" 0
end

extern @puts
