// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"

// IntegerFitting returns the smallest bit-size that the given
// literal value requires.  result will be 8, 16, 32 or 64.
// the result depends on the signed-ness, e.g. `255` only needs
// 8 bits for unsigned but 16-bits for signed.
func IntegerFitting(t *Node, unsigned bool) int {
	var err error

	if unsigned {
		_, err = strconv.ParseUint(t.str, 0, 8)
		if err == nil { return 8 }

		_, err = strconv.ParseUint(t.str, 0, 16)
		if err == nil { return 16 }

		_, err = strconv.ParseUint(t.str, 0, 32)
		if err == nil { return 32 }

	} else {
		_, err = strconv.ParseInt(t.str, 0, 8)
		if err == nil { return 8 }

		_, err = strconv.ParseInt(t.str, 0, 16)
		if err == nil { return 16 }

		_, err = strconv.ParseInt(t.str, 0, 32)
		if err == nil { return 32 }
	}

	return 64
}

func IntegerForceSize(t *Node, bits int) error {
	// Note: bits can be `1` to handle booleans.

	// we need to handle full range of s64 *and* u64, so parse twice
	v1, err1 := strconv.ParseInt (t.str, 0, 64)
	v2, err2 := strconv.ParseUint(t.str, 0, 64)

	if err1 != nil && err2 != nil {
		PostError("integer literal is too large")
		return FAILED
	}

	// WISH: if literal was hexadecimal, make new version hex too

	var new_val int64

	switch bits {
	case 1:
		if err1 != nil {
			new_val = int64(v2 & 1)
		} else if v1 < 0 || v1 > 1 {
			new_val = int64(v1 & 1)
		} else {
			return OK
		}
		t.str = strconv.FormatInt(new_val, 10)

	case 8:
		if err1 != nil {
			new_val = int64(uint8(v2))
		} else if v1 < -128 || v1 > 255 {
			new_val = int64(uint8(v1))
		} else {
			return OK
		}
		t.str = strconv.FormatInt(new_val, 10)

	case 16:
		if err1 != nil {
			new_val = int64(uint16(v2))
		} else if v1 < -0x8000 || v1 > 0xFFFF {
			new_val = int64(uint16(v1))
		} else {
			return OK
		}
		t.str = strconv.FormatInt(new_val, 10)

	case 32:
		if err1 != nil {
			new_val = int64(uint32(v2))
		} else if v1 < -0x80000000 || v1 > 0xFFFFFFFF {
			new_val = int64(uint32(v1))
		} else {
			return OK
		}
		t.str = strconv.FormatInt(new_val, 10)

	case 64:
		// leave it as-is
	}

	return OK
}
