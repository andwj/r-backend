// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

// Register represents a general purpose CPU register (when > 0).
// Exactly which CPU register it is depends on the architecture
// back-end.  Not all registers are interchangeable, e.g. some are
// floating point (like XMM0), some are integer/pointer (like RAX).
//
// Zero and negative values of Register have special meanings.
// REG_NONE is used when a register has not been decided yet.
// REG_ON_STACK is used in certain places for values that will be
// stored on (or moved to) the stack.  REG_CC represents the
// condition codes (though never for a local var).
type Register int

const REG_NONE     Register =  0
const REG_ON_STACK Register = -1
const REG_CC       Register = -2

type RegisterSet struct {
	group map[Register]bool
}

func NewRegisterSet() *RegisterSet {
	rs := new(RegisterSet)
	rs.group = make(map[Register]bool)
	return rs
}

func (rs *RegisterSet) Len() int {
	return len(rs.group)
}

func (rs *RegisterSet) Add(r Register) {
	rs.group[r] = true
}

func (rs *RegisterSet) Has(r Register) bool {
	return rs.group[r]
}

//----------------------------------------------------------------------

func (t *Node) IsRegister() bool {
	if t.kind == NP_Local {
		return t.local.reg != REG_ON_STACK
	}
	return false
}

func (t *Node) GetRegister() Register {
	if t.kind == NP_Local {
		if t.local.reg != REG_ON_STACK {
			return t.local.reg
		}
	}
	return REG_NONE
}

func (t *Node) UsesRegister(reg Register) bool {
	if t.kind == NP_Memory {
		t = t.children[0]
	}
	if t.kind == NP_Local {
		return t.local.reg == reg
	}
	return false
}
