// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "sort"

type StackLayout struct {
	// the current function
	fu *FuncDef

	// amount of stack currently used
	used int

	// optional behavior...
	reuse_locals   bool
	pack_locals    bool  // implied when re-using locals
	pack_stackvars bool
}

func NewStackLayout(fu *FuncDef) *StackLayout {
	st := new(StackLayout)
	st.fu = fu

	// TODO check for Options.optim (or so)
	st.reuse_locals   = true
	st.pack_locals    = true
	st.pack_stackvars = true

	return st
}

func StackAllocate(st *StackLayout) {
	// do local vars first (keep near base pointer)
	st.SA_Locals()

	// do stack-vars next
	st.SA_Blocks()

	// reserve space for on-stack parameters of functions *we* call
	st.SA_DeepestCall()
}

//----------------------------------------------------------------------

func (st *StackLayout) SA_DeepestCall() {
	// finds the function call with the most on-stack parameters,
	// and returns that quantity (as a number of slots).

	deepest := 0

	for _, t := range st.fu.body.children {
		comp := GetComputation(t)

		if comp != nil && comp.kind == NC_Call {
			// need arch-specific code here...
			depth := arch.CallDepth(comp)

			if depth > deepest {
				deepest = depth
			}
		}
	}

	st.SA_Alloc(deepest, 1)
}

//----------------------------------------------------------------------

type StackSlot struct {
	size    int
	locals  map[*LocalVar]bool

	// live range of all locals here
	live_start  int
	live_end    int
}

type SlotTable struct {
	slots  []*StackSlot
}

func NewStackSlot(size int) *StackSlot {
	ss := new(StackSlot)
	ss.size   = size
	ss.locals = make(map[*LocalVar]bool)
	ss.live_start = -1
	ss.live_end   = -1
	return ss
}

func NewSlotTable() *SlotTable {
	table := new(SlotTable)
	table.slots = make([]*StackSlot, 0)
	return table
}

func (ss *StackSlot) Add(local *LocalVar) {
	ss.locals[local] = true

	if ss.size < local.ty.size {
		ss.size = local.ty.size
	}

	if ss.live_start < 0 {
		ss.live_start = local.live_start
		ss.live_end   = local.live_end
	} else {
		if local.live_start < ss.live_start { ss.live_start = local.live_start }
		if local.live_end   > ss.live_end   { ss.live_end   = local.live_end   }
	}
}

func (table *SlotTable) Add(ss *StackSlot) {
	table.slots = append(table.slots, ss)
}

//----------------------------------------------------------------------

func (st *StackLayout) SA_Locals() {
	// locals which did not get a register get a stack slot here.

	// for a parameter passed on the stack, we use/modify it where it is
	// (in the caller's stack frame) rather than copy it into our own.
	// while that seems a bit cheeky, it matches what GCC does.

	// collect the unallocated locals
	list := make([]*LocalVar, 0)

	for _, local := range st.fu.locals {
		if local.reg == REG_ON_STACK && local.offset == 0 {
			list = append(list, local)
		}
	}

	if st.reuse_locals {
		// complex allocation
		st.SA_ReuseLocals(list)

	} else {
		// simple allocation
		if st.pack_locals {
			st.SA_SortLocalsBySize(list)
		}
		for _, local := range list {
			size  := local.ty.size
			align := CalcAlignment(size)

			local.offset = st.SA_Alloc(size, align)
		}
	}
}

func (st *StackLayout) SA_ReuseLocals(vars []*LocalVar) {
	// the maximum number of slots to search
	const MAX_LOOK = 64

	// table of stack slots
	table := NewSlotTable()

	// visit in declaration order
	for i := 0 ; i < len(vars) ; i++ {
		st.SA_InsertLocal(vars[i], table, MAX_LOOK)
	}

	// ensure smaller slots are done before bigger ones
	st.SA_SortSlots(table)

	// assign the offsets now
	for k := range table.slots {
		slot := table.slots[k]

		size   := slot.size
		align  := CalcAlignment(size)
		offset := st.SA_Alloc(size, align)

		for local, _ := range slot.locals {
			local.offset = offset
		}
	}
}

func (st *StackLayout) SA_InsertLocal(local *LocalVar, table *SlotTable, max_look int) {
	// look at the end section, which has the newest slots
	start := len(table.slots) - max_look
	if start < 0 {
		start = 0
	}

	var best *StackSlot = nil

	for k := len(table.slots)-1 ; k >= start ; k-- {
		slot := table.slots[k]

		usable := (local.live_start > slot.live_end) ||
		          (local.live_end   < slot.live_start)

		if usable {
			best = slot
			break
		}
	}

	if best != nil {
		best.Add(local)
		return
	}

	// no usable slots, need to create one
	slot := NewStackSlot(local.ty.size)
	slot.Add(local)

	table.Add(slot)
}

func (st *StackLayout) SA_SortSlots(table *SlotTable) {
	sort.Slice(table.slots, func(i, k int) bool {
		v1 := table.slots[i]
		v2 := table.slots[k]

		if v1.size != v2.size {
			return v1.size < v2.size
		}

		// tie breaker
		if v1.live_start != v2.live_start {
			return v1.live_start < v2.live_start
		}
		if v1.live_end != v2.live_end {
			return v1.live_end < v2.live_end
		}

		return len(v1.locals) > len(v2.locals)
	})
}


func (st *StackLayout) SA_SortLocalsBySize(list []*LocalVar) {
	sort.Slice(list, func(i, k int) bool {
		v1 := list[i]
		v2 := list[k]

		if v1.ty.size != v2.ty.size {
			return v1.ty.size < v2.ty.size
		}

		// tie breaker
		return v1.name < v2.name
	})
}

//----------------------------------------------------------------------

func (st *StackLayout) SA_Blocks() {
	// underneath the non-reg locals are the `stack-var` blocks.
	// blocks of 16 bytes or larger get a 16-byte alignment.

	// collect the NC_StackVar nodes
	nodes := make([]*Node, 0)

	for _, t := range st.fu.body.children {
		comp := GetComputation(t)

		if comp != nil && comp.kind == NC_StackVar {
			nodes = append(nodes, comp)
		}
	}

	// sort the sizes from smallest to largest
	if st.pack_stackvars {
		st.SA_SortStackVars(nodes)
	}

	for _, comp := range nodes {
		size   := int(comp.offset)
		align  := CalcAlignment(size)
		offset := st.SA_Alloc(size, align)

		comp.offset = int64(offset)
	}
}

func (st *StackLayout) SA_SortStackVars(nodes []*Node) {
	sort.Slice(nodes, func(i, k int) bool {
		t1 := nodes[i]
		t2 := nodes[k]

		if t1.offset != t2.offset {
			return t1.offset < t2.offset
		}

		// tie breaker
		return t1.pos.line < t2.pos.line
	})
}

//----------------------------------------------------------------------

func (st *StackLayout) SA_Alloc(size int, align int) int {
	// align must be a power of two.
	// size can be zero to merely align the stack.

	st.used = st.used + size

	if align > 1 {
		partial := st.used & (align - 1)

		if partial > 0 {
			st.used = st.used + align - partial
		}
	}

	return 0 - st.used
}

func CalcAlignment(size int) int {
	// for a given size, return a fairly normal alignment
	if size <= 1 { return 1 }
	if size <= 2 { return 2 }
	if size <= 4 { return 4 }
	if size <= 8 { return 8 }
	return 16
}
