// Copyright 2021 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

type BaseType int
const (
	TYP_Void BaseType = iota
	TYP_Bool
	TYP_Int
	TYP_Float
	TYP_Pointer
)

type Type struct {
	name string

	kind BaseType

	// for TYP_Int and TYP_Float, this is number of bits (8,16,32,64).
	// for TYP_Pointer this depends on the architecture.
	bits int

	// the total size of this type (in bytes).
	size int

	// true for types whose size is architecture-dependent.
	archy bool
}

//----------------------------------------------------------------------

var i8_type  *Type
var i16_type *Type
var i32_type *Type
var i64_type *Type

var f32_type *Type
var f64_type *Type

var  ptr_type *Type
var iptr_type *Type

var bool_type *Type
var void_type *Type

func InitTypes() {
	reg_bits := arch.RegisterBits()

	i8_type  = NewType( "i8", TYP_Int,  8, false)
	i16_type = NewType("i16", TYP_Int, 16, false)
	i32_type = NewType("i32", TYP_Int, 32, false)
	i64_type = NewType("i64", TYP_Int, 64, false)

	f32_type = NewType("f32", TYP_Float, 32, false)
	f64_type = NewType("f64", TYP_Float, 64, false)

	 ptr_type = NewType( "ptr", TYP_Pointer, reg_bits, true)
	iptr_type = NewType("iptr", TYP_Int,     reg_bits, true)

	bool_type = NewType("bool", TYP_Bool, 8, false)
	void_type = NewType("void", TYP_Void, 0, false)
}

func NewType(name string, kind BaseType, bits int, archy bool) *Type {
	ty := new(Type)

	ty.name  = name
	ty.kind  = kind
	ty.bits  = bits
	ty.size  = bits / 8
	ty.archy = archy

	return ty
}

//----------------------------------------------------------------------

func ParseType(t *Node) *Type {
	if t.kind == NL_Name && t.sigil == 0 {
		name := t.str
		ty   := LookupType(name)
		if ty == nil {
			PostError("no such type: %s", name)
			return nil
		}
		return ty
	}
	PostError("expected type name, got: %s", t.String())
	return nil
}

func LookupType(name string) *Type {
	switch name {
	case "i8":  return i8_type
	case "i16": return i16_type
	case "i32": return i32_type
	case "i64": return i64_type

	case "f32": return f32_type
	case "f64": return f64_type

	case  "ptr": return  ptr_type
	case "iptr": return iptr_type

	case "bool": return bool_type
	case "void": return void_type
	}

	return nil
}

func TypeCompare(src *Type, dest *Type) bool {
	if src.kind  != dest.kind  { return false }
	if src.bits  != dest.bits  { return false }
	if src.archy != dest.archy { return false }

	return true
}

//----------------------------------------------------------------------

func (ty *Type) String() string {
	return ty.name
}

func (ty *Type) SimpleName() string {
	return ty.kind.String()
}

func (kind BaseType) String() string {
	switch kind {
	case TYP_Void:     return "void"
	case TYP_Bool:     return "boolean"
	case TYP_Int:      return "integer"
	case TYP_Float:    return "float"
	case TYP_Pointer:  return "pointer"
	default:
		panic("unknown type kind")
	}
}
