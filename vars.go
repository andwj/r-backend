// Copyright 2022 Andrew Apted.
// Use of this code is governed by an MIT-style license.
// See the top-level "LICENSE.md" file for the full text.

package main

import "strconv"

func ParseEverything() error {
	for _, def := range ord_defs {
		switch def.kind {
			case DEF_Var:  def.d_var .Parse()
			case DEF_Func: def.d_func.Parse()
		}
	}

	if have_errors {
		return FAILED
	}
	return OK
}

//----------------------------------------------------------------------

func (va *VarDef) Parse() {
	t := va.body

	Error_SetPos(t.pos)

	va.data = NewNode(NG_Block, "", t.pos)

	va.cur_type = nil

	if t.kind == NG_Line {
		va.Var_Single(t)
	} else {
		va.Var_Block(t)
	}
}

func (va *VarDef) Var_Single(t *Node) error {
	if t.Len() != 2 {
		PostError("malformed data value (not two elements)")
		return FAILED
	}

	// check that first keyword is valid
	t_type := t.children[0]

	if ! t_type.Match("zero") {
		if ParseType(t_type) == nil {
			return FAILED
		}
	}

	return va.Var_Line(t)
}

func (va *VarDef) Var_Block(t *Node) error {
	for _, line := range t.children {
		if va.Var_Line(line) != OK {
			return FAILED
		}
	}
	return OK
}

func (va *VarDef) Var_Line(line *Node) error {
	Error_SetPos(line.pos)

	children := line.children

	for len(children) > 0 {
		t_elem  := children[0]
		children = children[1:]

		// zero filling?
		if t_elem.Match("zero") {
			if len(children) < 1 {
				PostError("missing size value after zero")
				return FAILED
			}

			t_size  := children[0]
			children = children[1:]

			if va.Var_Zero(t_size) != OK {
				return FAILED
			}
			continue
		}

		// a type name?
		if t_elem.kind == NL_Name && t_elem.sigil == 0 {
			va.cur_type = ParseType(t_elem)
			if va.cur_type == nil {
				return FAILED
			}
			continue
		}

		if va.cur_type == nil {
			PostError("bad data (missing type name)")
			return FAILED
		}

		if va.Var_Element(t_elem) != OK {
			return FAILED
		}
	}

	return OK
}

func (va *VarDef) Var_Zero(t_value *Node) error {
	if t_value.kind != NL_Integer {
		PostError("expected integer after zero, got: %s", t_value.String())
		return FAILED
	}

	size, err := strconv.ParseInt(t_value.str, 0, 64)
	if err != nil || size <= 0 {
		PostError("illegal size after zero: %s", t_value.str)
		return FAILED
	}

	t_zero := NewNode(ND_Zeroes, "", t_value.pos)
	t_zero.offset = size

	va.data.Add(t_zero)
	return OK
}

func (va *VarDef) Var_Element(t *Node) error {
	ty := va.cur_type

	if t.kind == NL_Name {
		if t.sigil != '@' {
			PostError("unexpected identifier in data")
			return FAILED
		}
		def := all_defs[t.str]
		if def == nil {
			PostError("no such global: @%s", t.str)
			return FAILED
		}
		if ty.kind != TYP_Pointer {
			PostError("global @%s used in non-pointer context", t.str)
			return FAILED
		}

		t_elem := NewNode(NP_GlobPtr, "", t.pos)
		t_elem.ty = ty
		t_elem.def = def

		va.data.Add(t_elem)
		return OK
	}

	if t.kind == NL_String && ty.kind == TYP_Int {
		if ty.archy || ty.bits > 32 {
			PostError("strings require i8, i16 or i32 type")
			return FAILED
		}
		// decompose string into characters
		switch ty.bits {
			case 16: va.String_UTF16(t, ty)
			case 32: va.String_UTF32(t, ty)
			default: va.String_UTF8 (t, ty)
		}
		return OK
	}

	t_elem := TypedLiteral(t, ty)
	if t_elem == P_FAIL {
		return FAILED
	}

	va.data.Add(t_elem)
	return OK
}

//----------------------------------------------------------------------

func TypedLiteral(t *Node, ty *Type) *Node {
	t.ty = ty

	switch ty.kind {
		case TYP_Void:
			PostError("cannot use void in that context")
			return P_FAIL

		case TYP_Int:
			switch t.kind {
			case NL_Integer, NL_Char:
				if IntegerForceSize(t, ty.bits) != OK {
					return P_FAIL
				}
				return t

			default:
				PostError("expected integer value, got: %s", t.String())
				return P_FAIL
			}

		case TYP_Float:
			switch t.kind {
			case NL_Float:
				// check the value is valid for the type
				var err error
				if ty.bits == 32 {
					_, err = strconv.ParseFloat(t.str, 32)
				} else {
					_, err = strconv.ParseFloat(t.str, 64)
				}
				if err != nil {
					PostError("float value is too large for a f%d", ty.bits)
					return P_FAIL
				}
				return t

			case NL_FltSpec:
				return t

			case NL_Integer:
				// a raw representation of a float
				if IntegerForceSize(t, ty.bits) != OK {
					return P_FAIL
				}
				return t

			default:
				PostError("expected float value, got: %s", t.String())
				return P_FAIL
			}

		case TYP_Bool:
			switch t.kind {
			case NL_Integer:
				if IntegerForceSize(t, 1) != OK {
					return P_FAIL
				}
				return t

			default:
				PostError("expected boolean value, got: %s", t.String())
				return P_FAIL
			}

		case TYP_Pointer:
			switch t.kind {
			case NL_Integer:
				// a raw address (often zero)
				if IntegerForceSize(t, ty.bits) != OK {
					return P_FAIL
				}
				return t

			default:
				PostError("expected pointer value, got: %s", t.String())
				return P_FAIL
			}

		default:
			panic("weird type")
	}
}

//----------------------------------------------------------------------

func (va *VarDef) String_UTF8(t *Node, ty *Type) {
	// read individual bytes of string (NOT unicode points).
	// the input string is allowed to contain sequences which are
	// not valid UTF-8.

	s := t.str

	for pos := 0; pos < len(s); pos++ {
		var ch byte = s[pos]

		if ch >= 128 {
			va.AppendHex(uint64(ch), ty)
		} else {
			va.AppendUnicode(rune(ch), ty)
		}
	}
}

func (va *VarDef) String_UTF16(t *Node, ty *Type) {
	// read unicode characters of string
	for _, ch := range t.str {
		va.AppendUnicode(ch, ty)
	}
}

func (va *VarDef) String_UTF32(t *Node, ty *Type) {
	// read unicode characters of string
	for _, ch := range t.str {
		va.AppendUnicode(ch, ty)
	}
}

func (va *VarDef) AppendUnicode(ch rune, ty *Type) {
	// for UTF-16, decompose large value into a surrogate pair.
	// [ any surrogate pairs in the input are passed as-is ]
	if (ty.bits == 16) && (ch > 0xFFFF) {
		ch   -= 0x10000
		high :=  0xD800 + ((ch >> 10) & 0x03FF)
		low  :=  0xDC00 + ( ch        & 0x03FF)

		va.AppendHex(uint64(high), ty)
		va.AppendHex(uint64(low),  ty)
		return
	}

	int_str := strconv.FormatInt(int64(ch), 10)
	t_elem := NewNode(NL_Char, int_str, Position{})
	t_elem.ty = ty

	va.data.Add(t_elem)
}

func (va *VarDef) AppendHex(n uint64, ty *Type) {
	hex_str := "0x" + strconv.FormatUint(n, 16)
	t_elem := NewNode(NL_Integer, hex_str, Position{})
	t_elem.ty = ty

	va.data.Add(t_elem)
}
